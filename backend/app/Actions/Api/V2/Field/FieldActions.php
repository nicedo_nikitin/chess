<?php
namespace App\Actions\Api\V2\Field;
use App\Models\Api\V2\Field;

/**
*Класс выводит возможные поля по порталу(цена, этаж и т.д.)
*/
class FieldActions
{	
	/**
	*коллекция полей
	*@return use App\Models\Api\V2\Field
	*/
	public static function getFieldsCollections()
	{
		try {
			return Field::all();
		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
	*поля
	*@return array
	*/
	public static function getFields(): array
	{
		try {
			return self::getFieldsCollections()->toArray();
		} catch (Exception $e) {
			abort(500);
		}
	}
}