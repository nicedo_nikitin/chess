<?php
namespace App\Actions\Api\V2\Filter;
use Illuminate\Support\Arr;
use App\Actions\Api\V2\SettingFilterUser\SettingFilterUserActions;
use App\Actions\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalActions;

/**
*Класс по настройкам полей фильтра
*/
class FilterActions
{
    /**
    *Возращает поля по фильтру, а также если есть настройки пользователя по фильтру
    *Для цены, общая площадь создаем поля (от и до)
    *Для остальных поиск по значению  
    *@return array
    */
    public static function getFilter(): array
    {   
        try {
            $arrResult = [];

            $getSettingFilterUser = SettingFilterUserActions::getSettingFilterUser();

            $getSettingsFieldsPortal = SettingsFieldsPortalActions::getSettingsFieldsPortal();

            for($i = 0; $i < count($getSettingFilterUser); $i++) {

                $active = $getSettingFilterUser[$i]['active'];

                if($active)
                {   
                    $field = $getSettingFilterUser[$i]['relationships']['field'];

                    $name = $field['name'];

                    $const = $field['const'];

                    $settingsFieldsPortalId = $getSettingFilterUser[$i]['settings_fields_portal_id'];

                    $settingFiledPortal = Arr::first($getSettingsFieldsPortal, function ($value, $key) use($settingsFieldsPortalId) {
                       if(isset($value['relationships']['settingFieldPortal']['id'])) return $value['relationships']['settingFieldPortal']['id'] == $settingsFieldsPortalId;
                    });

                    if(is_null($settingFiledPortal)) continue;

                    $bxFields = $settingFiledPortal['relationships']['settingFieldPortal']['relationships']['bxField'];

                    $bxFieldConst = $bxFields["const"];
                    
                    $typeBx = $bxFields['type']; 

                    if(in_array($const, ['price', 'total_area']))
                    {
                       $arrResult[] = self::getFieldFrom($name, $const, $typeBx);
                       $arrResult[] = self::getFieldTo($name, $const, $typeBx);
                    }
                    else
                    {
                        $arrResult[] = self::getField($name, $const, $typeBx);
                    }
                }
            }

            return $arrResult;
            
        } catch (\Exception $e) {
            abort(500);
        }
    }

    private static function getField(string $name, string $const, string $type): array
    {   
        try {
            return [
                'name' => $name,
                'const' => $const,
                'type' => $type
            ];
            
        } catch (Exception $e) {
             abort(500); 
        }
    }

    private static function getFieldFrom(string $name, string $const, string $type): array
    {   
        try {
            return [
                'name' => $name.' от',
                'const' => $const.'From',
                'type' => $type
            ];
            
        } catch (Exception $e) {
            abort(500);  
        }
    }

    private static function getFieldTo(string $name, string $const, string $type): array
    {
        try {
            return [
                'name' => $name.' до',
                'const' => $const.'To',
                'type' => $type
            ];
            
        } catch (Exception $e) {
            abort(500);
        }
    }
}