<?php
namespace App\Actions\Api\V2\Portal;
use App\Models\Api\V2\Portal;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Actions\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalActions;

/**
*Класс по информации, настройкам портала
*/
class PortalActions
{
    /**
    *Проверяет наличие настроек по порталу
    *@return bool
    */
    public static function checkSettings(): bool
    {
        try {
            return SettingsFieldsPortalActions::checkSettings();
        } catch (Exception $e) {
            abort(500); 
        }
    }

    /**
    *Проверяет наличие настроек по порталу и если нет создает их
    *@return bool
    */
    public static function createSettings()
    {
        try {
            if(!self::checkSettings()) SettingsFieldsPortalActions::createSettings(); 
        } catch (\Exception $e) {
            abort(500);
        }
    }

    /**
    *Получаем инфо с бд по порталу
    *domain дешифруем
    *@param int $portalId
    *@return array
    */
    public static function getPortal(int $portalId): array
    {   
        try {

            $arrResult = [];

            $portal = Portal::find($portalId);

            foreach ($portal->toArray() as $key => $value) {
                
                if(in_array($key, ['domain']))  $arrResult[$key] = Crypt::decryptString($value);
                else $arrResult[$key] = $value;
            }
            return $arrResult;
            
        } catch (\Exception $e) {
            abort(500);
        }
    }

    /**
    *Получаем id с бд по порталу
    *@param string $memberId
    *@return int
    */
	public static function getPortalId(string $memberId): int
	{   try {
            return Portal::where('member_id', $memberId)->first()->id;
        } catch (Exception $e) {
            abort(500); 
        }
	}

    /**
    *Метод для создания или редактирования портала
    *domain шифруем
    *@param string $memberId
    *@param string $domain
    *@return int
    */
	public static function updateOrCreate(string $memberId, string $domain): int
    {
        DB::beginTransaction();

        try{
            $portal = Portal::updateOrCreate(['member_id' => $memberId], ['domain' => Crypt::encryptString($domain)]);
            DB::commit();
            return $portal->id;
        }catch(\Exception $ex){
            DB::rollBack();
            abort(500);
        }
    }
}