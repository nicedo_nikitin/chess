<?php
namespace App\Actions\Api\V2\Product;
use Illuminate\Support\Arr;
use App\Repository\Bitrix\BitrixRepository;
use App\Actions\Api\V2\SettingDetailcardUser\SettingDetailcardUserActions;
use App\Actions\Api\V2\SettingMinicardUser\SettingMinicardUserActions;
use App\Actions\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalActions;
use App\Repository\Consts\ConstsRepository;

/**
*Класс по информации, настройкам товаров в Битрикс24
*/
class ProductActions
{   
    private static function getDefaultFiledBx(): array
    {   
        $arrResult = [];

        $arrResult['id'] = [
            'name' => 'id',
            'const' => 'id',
            'order' => 0,
            'bxConst' => ConstsRepository::getKeysBitrixProducts()['ID'],
            'values' => []
        ];

        $arrResult['sectionId'] = [
            'name' => 'sectionId',
            'const' => 'sectionId',
            'order' => 0,
            'bxConst' => ConstsRepository::getKeysBitrixProducts()['SECTION_ID'],
            'values' => []
        ];

        $arrResult['catalogId'] = [
            'name' => 'catalogId',
            'const' => 'catalogId',
            'order' => 0,
            'bxConst' => ConstsRepository::getKeysBitrixProducts()['CATALOG_ID'],
            'values' => []
        ];

        return  $arrResult;
    }

    private static function getFilterKey(string $filedName, string $filterSign = ''): string
    {
        try {
            $sign = '';
            if($filterSign == 'From') $sign = '>=';
            elseif($filterSign == 'To') $sign = '<=';
            return $sign.$filedName;
           
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
    *Получаем товары с Битрикс24
    *Выводим поля в зависимости от настройки миникарточки пользователя
    *Формируем фильтр для запроса к Битрикс24 по товарам
    *@param array $filter
    *@param int $page
    *@return array
    */
    public static function index(array $filter, int $page): array
    {
        $fieldsBx = [];

        $select = [];

        $fieldsBx = self::getDefaultFiledBx();

        $getSettingMinicardUser = SettingMinicardUserActions::getSettingMinicardUser();

        $getSettingsFieldsPortal = SettingsFieldsPortalActions::getSettingsFieldsPortal();

        for($i = 0; $i < count($getSettingMinicardUser); $i++) {

            $active = $getSettingMinicardUser[$i]['active'];

            if($active)
            {   
                $field = $getSettingMinicardUser[$i]['relationships']['field'];

                $name = $field['name'];

                $const = $field['const'];

                $order = $getSettingMinicardUser[$i]['order'];

                $settingsFieldsPortalId = $getSettingMinicardUser[$i]['settings_fields_portal_id'];

                $settingFiledPortal = Arr::first($getSettingsFieldsPortal, function ($value, $key) use($settingsFieldsPortalId) {
                   if(isset($value['relationships']['settingFieldPortal']['id'])) return $value['relationships']['settingFieldPortal']['id'] == $settingsFieldsPortalId;
                });

                $bxFields = $settingFiledPortal['relationships']['settingFieldPortal']['relationships']['bxField'];

                if(is_null($settingFiledPortal)) continue;

                $select[] = $bxFields["const"];

                $fieldsBx[$const] = [
                    'name' => $name,
                    'const' => $const,
                    'order' => $order,
                    'bxConst' => $bxFields['const'],
                    'values' => []
                ];
            }
        }

        $filterBx = [];

        foreach($fieldsBx as $keyFieldsBx => $valueFieldsBx)
        {
            if(isset($filter[$valueFieldsBx['const']])) $filterBx[static::getFilterKey($valueFieldsBx['bxConst'])] = $filter[$valueFieldsBx['const']];

            if(isset($filter[$valueFieldsBx['const'].'From'])) $filterBx[static::getFilterKey($valueFieldsBx['bxConst'], 'From')] = $filter[$valueFieldsBx['const'].'From'];

            if(isset($filter[$valueFieldsBx['const'].'To'])) $filterBx[static::getFilterKey($valueFieldsBx['bxConst'], 'To')] = $filter[$valueFieldsBx['const'].'To'];
        }

        $getProducts = BitrixRepository::getProducts($select, $filterBx, $page);

        if(isset($getProducts['result']))
        {   
            $arrResult = [];

            for($i = 0; $i < count($getProducts['result']); $i++)
            {
                foreach ($fieldsBx as $keyFieldBx => $valueFieldBx) {
                    if(isset($getProducts['result'][$i][$valueFieldBx['bxConst']]))
                    {
                        $arrResult['result'][$i][$valueFieldBx['const']] = $valueFieldBx;
                        $arrResult['result'][$i][$valueFieldBx['const']]['values'] = $getProducts['result'][$i][$valueFieldBx['bxConst']];  
                    } 
                }
            }   
        }

        $arrResult['pagination'] = $getProducts['pagination'];
        
       return $arrResult;
    }

    /**
    *Получаем товары с Битрикс24
    *Выводим поля в зависимости от настройки детальной карточки пользователя
    *@param int $productId
    *@return array
    */
    public static function show(int $productId): array
    {   
        try {

            $arrResult = [];

            $arrResult = self::getDefaultFiledBx();


            $select = [];

            $filter = ['ID' => $productId];

            $getSettingDetailcardUser = SettingDetailcardUserActions::getSettingDetailcardUser();



            $getSettingsFieldsPortal = SettingsFieldsPortalActions::getSettingsFieldsPortal();

            for($i = 0; $i < count($getSettingDetailcardUser); $i++) {

                $active = $getSettingDetailcardUser[$i]['active'];
                
                if($active)
                {   
                    $order = $getSettingDetailcardUser[$i]['order'];

                    $field = $getSettingDetailcardUser[$i]['relationships']['field'];

                    $name = $field['name'];

                    $const = $field['const'];

                    $settingsFieldsPortalId = $getSettingDetailcardUser[$i]['settings_fields_portal_id'];

                    $settingFiledPortal = Arr::first($getSettingsFieldsPortal, function ($value, $key) use($settingsFieldsPortalId) {
                       if(isset($value['relationships']['settingFieldPortal']['id'])) return $value['relationships']['settingFieldPortal']['id'] == $settingsFieldsPortalId;
                    });

                    if(is_null($settingFiledPortal)) continue;

                    $bxFields = $settingFiledPortal['relationships']['settingFieldPortal']['relationships']['bxField'];

                    $select[] = $bxFields["const"];

                    $arrResult[$const] = [
                        'name' => $name,
                        'const' => $const,
                        'order' => $order,
                        'bxConst' => $bxFields['const'],
                        'values' => []
                    ];
                }
            }

            $getProducts = BitrixRepository::getProducts($select, $filter);

            if(isset($getProducts['result'][0]))
            {
                foreach ($arrResult as $keyArrResult => $value) {
                    if(isset($getProducts['result'][0][$value['bxConst']])) $arrResult[$keyArrResult]['values'] = $getProducts['result'][0][$value['bxConst']];
                }

                return $arrResult;
            }
            
            return [];
                        
        } catch (\Exception $e) {
            abort(500);
        }
    }
}