<?php
namespace App\Actions\Api\V2\SettingDetailcardUser;
use Illuminate\Support\Facades\DB;
use App\Actions\Api\V2\Field\FieldActions;
use App\Actions\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalActions;
use App\Models\Api\V2\SettingDetailcardUser;
use App\Actions\Api\V2\User\UserActions;

/**
*Класс по информации, настройкам детальной карточки
*Для каждого пользователя своя настройка
*/
class SettingDetailcardUserActions
{	
	/**
    *Получаем настройки детальной карточки по пользователю
    *@return array
    */
	public static function getSettingDetailcardUser(): array
	{
		try {
			
			$arrResult = [];

			$fields = FieldActions::getFieldsCollections();

			$i = 0;

			for($iField = 0; $iField < $fields->count(); $iField++)
			{	
				$keyField = $fields[$iField];

				$getSettingFieldPortalCollections = SettingsFieldsPortalActions::getSettingFieldPortalCollections($keyField->id);

				if(!empty($getSettingFieldPortalCollections)) 
				{	
					$settingDetailcard = $getSettingFieldPortalCollections->settingDetailcard;

					$arrResult[$i]['active'] = false;

					if(!is_null($settingDetailcard))
					{
						$arrResult[$i]['active'] = true;
						
						$arrResult[$i]['order'] = $settingDetailcard->order;

						$arrResult[$i] = array_merge($arrResult[$i], $settingDetailcard->toArray());
					} 

					$arrResult[$i]['relationships']['field'] = $fields[$iField]->toArray();

					$i++;
				}
			}
			
			return $arrResult;
			
		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Получаем настройки детальной карточки по пользователю
    *@return use App\Models\Api\V2\SettingDetailcardUser;
    */
	public static function getSettingDetailcardUserCollections()
	{
		try {
			
			return SettingDetailcardUser::where(['user_id' => UserActions::getUserId()]);

		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Метод по удалению или созданию настройки детальной карточки по пользователю
    *@param bool $active
    *@param int $fieldId
    *@return bool
    */
	public static function deleteOrCreate(bool $active, int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			DB::commit();

			if($active) return self::create($fieldId);
			
			return self::delete($fieldId);
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по автоматическому созданию настройки детальной карточки по пользователю
    *@return bool
    */
    public static function createSettings(): void
	{
		DB::beginTransaction();

		try {

			if(self::getSettingDetailcardUserCollections()->count() == 0)
			{
				$fields = FieldActions::getFieldsCollections();

				for($iField = 0; $iField < $fields->count(); $iField++) if(!empty(SettingsFieldsPortalActions::getSettingFieldPortalCollections($fields[$iField]->id))) self::create($fields[$iField]->id);
			}

			DB::commit();
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
	}

	/**
    *Метод по созданию настройки детальной карточки по пользователю
    *@param int $fieldId
    *@return bool
    */
    public static function create(int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			$getSettingFieldPortalCollections = SettingsFieldsPortalActions::getSettingFieldPortalCollections($fieldId);

			$settingDetailcard = self::getSettingDetailcardCollection($fieldId);
			
			if(!is_null($getSettingFieldPortalCollections) && is_null($settingDetailcard))
			{	
				DB::commit();

				SettingDetailcardUser::create([
					'user_id' => UserActions::getUserId(),
					'settings_fields_portal_id' => $getSettingFieldPortalCollections->id,
					'order' => self::getSettingDetailcardUserCollections()->count() + 1 
				]);

				return true;
			}

			return false;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по сортировке настройки детальной карточки по пользователю
	*@param int $order
    *@param int $fieldId
    *@return bool
    */
    public static function order(int $order, int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			$settingDetailcard = self::getSettingDetailcardCollection($fieldId);

			if(!is_null($settingDetailcard))
			{	
				DB::commit();

				self::refreshFieldshOrder($settingDetailcard->id, $order);

				return true;
			}

			return false;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по обновлению порядка настроек детальной карточки по пользователю
    *@param int $id
    *@param int $changeFieldOrder = 0
    */
    private static function refreshFieldshOrder(int $id, int $changeFieldOrder = 0)
    {
    	DB::beginTransaction();

		try {

			DB::commit();

			if($changeFieldOrder > 0) self::getSettingDetailcardUserCollections()->where('id', $id)->update(['order' => $changeFieldOrder]);

			$getSettingDetailcardUserCollections = self::getSettingDetailcardUserCollections()->where('id' ,'!=', $id)->get();

			$count = $getSettingDetailcardUserCollections->count();

			for($i = 0; $i < $count; $i++)
			{	
				$order = $i + 1;

				if($changeFieldOrder > 0)
				{
					if($changeFieldOrder > $order) $order = $i+1;
					elseif($changeFieldOrder <= $order) $order = $i+2;	
				}
				
				$getSettingDetailcardUserCollections[$i]->update(['order' => $order]);
			}
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по удалению порядка настроек детальной карточки по пользователю
    *@param int $fieldId
    *@return bool
    */
    public static function delete(int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			$settingDetailcard = self::getSettingDetailcardCollection($fieldId);

			if(!is_null($settingDetailcard) && self::getSettingDetailcardUserCollections()->count() > 1)
			{	
				DB::commit();

				$settingDetailcard->delete();

				self::refreshFieldshOrder($settingDetailcard->id, 0);

				return true;
			}

			return false;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    } 

    /**
    *Метод проверяет есть ли настрока по порталу
    *Например если не настроили поле цену то нельзя использовать это поле в настройках по пользователю 
    *@param int $fieldId
    *@return ?use App\Actions\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalActions;
    */
    private static function getSettingDetailcardCollection(int $fieldId)
    {
    	$getSettingFieldPortalCollections = SettingsFieldsPortalActions::getSettingFieldPortalCollections($fieldId);

		$settingDetailcard = (!is_null($getSettingFieldPortalCollections)) ? $getSettingFieldPortalCollections->settingDetailcard : null;

		return $settingDetailcard;
    }
}