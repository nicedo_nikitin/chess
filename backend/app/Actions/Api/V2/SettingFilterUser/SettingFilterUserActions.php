<?php
namespace App\Actions\Api\V2\SettingFilterUser;
use Illuminate\Support\Facades\DB;
use App\Actions\Api\V2\Field\FieldActions;
use App\Actions\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalActions;
use App\Actions\Api\V2\User\UserActions;
use App\Models\Api\V2\SettingFilterUser;

/**
*Класс по информации, настройкам фильтра
*Для каждого пользователя своя настройка
*/
class SettingFilterUserActions
{
	/**
    *Получаем настройки фильтра по пользователю
    *@return array
    */
	public static function getSettingFilterUser(): array
	{
		try {
			
			$arrResult = [];

			$fields = FieldActions::getFieldsCollections();
			
			$i = 0;

			for($iField = 0; $iField < $fields->count(); $iField++)
			{	
				$keyField = $fields[$iField];

				$getSettingFieldPortalCollections = SettingsFieldsPortalActions::getSettingFieldPortalCollections($keyField->id);

				$typesField = $keyField->typesField->pluck('const')->toArray();

				if(!empty($getSettingFieldPortalCollections) && (in_array('integer', $typesField) || in_array('double', $typesField) || in_array('string', $typesField))) 
				{	
					$settingFilter = $getSettingFieldPortalCollections->settingFilter;

					if(!is_null($settingFilter)) $arrResult[$i] = $settingFilter->toArray();

					$arrResult[$i]['active'] = (!is_null($settingFilter)) ? true : false;

					$arrResult[$i]['relationships']['field'] = $fields[$iField]->toArray();

					$i++;
				}
			}

			return $arrResult;
			
		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Получаем настройкифильтра по пользователю
    *@return use App\Models\Api\V2\SettingFilterUser;
    */
	public static function getSettingFilterUserCollections()
	{
		try {
			
			return SettingFilterUser::where(['user_id' => UserActions::getUserId()]);

		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Метод по удалению или созданию настройки фильтра по пользователю
    *@return bool
    */
	public static function deleteOrCreate(bool $active, int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			DB::commit();

			if($active) return self::create($fieldId);
			
			return self::delete($fieldId);
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по автоматическому созданию фильтра карточки по пользователю
    *@return bool
    */
    public static function createSettings(): void
	{
		DB::beginTransaction();

		try {

			if(self::getSettingFilterUserCollections()->count() == 0)
			{
				$fields = FieldActions::getFieldsCollections();

				for($iField = 0; $iField < $fields->count(); $iField++)
				{
					$typesField = $fields[$iField]->typesField->pluck('const')->toArray();
					
					if(!empty(SettingsFieldsPortalActions::getSettingFieldPortalCollections($fields[$iField]->id)) && (in_array('integer', $typesField) || in_array('double', $typesField) || in_array('string', $typesField)))
					{
						self::create($fields[$iField]->id);
					} 
				} 
			}

			DB::commit();
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
	}

	/**
    *Метод по созданию настройки фильтра по пользователю
    *@return bool
    */
    public static function create(int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			$getSettingFieldPortalCollections = SettingsFieldsPortalActions::getSettingFieldPortalCollections($fieldId);

			$settingFilter = self::getSettingFilterCollection($fieldId);
			
			if(!is_null($getSettingFieldPortalCollections) && is_null($settingFilter))
			{	
				DB::commit();

				SettingFilterUser::create([
					'user_id' => UserActions::getUserId(),
					'settings_fields_portal_id' => $getSettingFieldPortalCollections->id,
					'order' => self::getSettingFilterUserCollections()->count() + 1 
				]);

				return true;
			}

			return false;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по обновлению порядка настроек фильтра по пользователю
    */
    private static function refreshFieldshOrder(int $changeOrderFieldId)
    {
    	DB::beginTransaction();

		try {

			DB::commit();

			$getSettingFilterUserCollections = self::getSettingFilterUserCollections()->get();

			for($i = 0; $i < $getSettingFilterUserCollections->count(); $i++)
			{
				$order = $i + 1;

				$getSettingFilterUserCollections[$i]->update(['order' => $order]);
			}
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по удалению порядка настроек фильтра по пользователю
    */
    public static function delete(int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			$settingFilter = self::getSettingFilterCollection($fieldId);

			if(!is_null($settingFilter) && self::getSettingFilterUserCollections()->count() > 1)
			{	
				DB::commit();

				$settingFilter->delete();

				self::refreshFieldshOrder($fieldId);

				return true;
			}

			return false;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    } 

    /**
    *Метод проверяет есть ли настрока по порталу
    *Например если не настроили поле цену то нельзя использовать это поле в настройках по пользователю 
    */
    private static function getSettingFilterCollection(int $fieldId)
    {
    	$getSettingFieldPortalCollections = SettingsFieldsPortalActions::getSettingFieldPortalCollections($fieldId);

		$settingFilter = (!is_null($getSettingFieldPortalCollections)) ? $getSettingFieldPortalCollections->settingFilter : null;

		return $settingFilter;
    }
}