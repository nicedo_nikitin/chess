<?php
namespace App\Actions\Api\V2\SettingMinicardUser;
use Illuminate\Support\Facades\DB;
use App\Actions\Api\V2\Field\FieldActions;
use App\Actions\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalActions;
use App\Models\Api\V2\SettingMinicardUser;
use App\Actions\Api\V2\User\UserActions;

/**
*Класс по информации, настройкам мини карточки
*Для каждого пользователя своя настройка
*/
class SettingMinicardUserActions
{
	/**
    *Получаем настройки мини карточки по пользователю
    *@return array
    */
	public static function getSettingMinicardUser(): array
	{
		try {

			$arrResult = [];

			$fields = FieldActions::getFieldsCollections();

			$i = 0;

			for($iField = 0; $iField < $fields->count(); $iField++)
			{	
				$keyField = $fields[$iField];

				$getSettingFieldPortalCollections = SettingsFieldsPortalActions::getSettingFieldPortalCollections($keyField->id);

				$typesField = $keyField->typesField->pluck('const')->toArray();

				if(!empty($getSettingFieldPortalCollections) && (in_array('integer', $typesField) || in_array('double', $typesField) || in_array('string', $typesField))) 
				{	
					$settingMinicard = $getSettingFieldPortalCollections->settingMinicard;

					$arrResult[$i]['active'] = false;

					if(!is_null($settingMinicard))
					{
						$arrResult[$i]['active'] = true;
						
						$arrResult[$i]['order'] = $settingMinicard->order;

						$arrResult[$i] = array_merge($arrResult[$i], $settingMinicard->toArray());
					} 

					$arrResult[$i]['relationships']['field'] = $fields[$iField]->toArray();

					$i++;
				}
			}

			return $arrResult;
			
		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Получаем настройки мини карточки по пользователю
    *@return use App\Actions\Api\V2\SettingsFieldsPortal;
    */
	public static function getSettingMinicardUserCollections()
	{
		try {
			
			return SettingMinicardUser::where(['user_id' => UserActions::getUserId()])->orderBy('order');

		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Метод по удалению или созданию настройки мини карточки по пользователю
    *@return bool
    */
	public static function deleteOrCreate(bool $active, int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			DB::commit();

			if($active) return self::create($fieldId);
			
			return self::delete($fieldId);
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по автоматическому созданию настройки мини карточки по пользователю
    *@return bool
    */
    public static function createSettings(): void
	{
		DB::beginTransaction();

		try {

			if(self::getSettingMinicardUserCollections()->count() == 0)
			{
				$fields = FieldActions::getFieldsCollections();

				$iteration = 0;

				for($iField = 0; $iField < $fields->count(); $iField++)
				{
					$typesField = $fields[$iField]->typesField->pluck('const')->toArray();

					if(!empty(SettingsFieldsPortalActions::getSettingFieldPortalCollections($fields[$iField]->id)) && $iteration < 5 && (in_array('integer', $typesField) || in_array('double', $typesField) || in_array('string', $typesField)))
					{
						self::create($fields[$iField]->id);
						$iteration++;
					} 
				} 
			}

			DB::commit();
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
	}

	/**
    *Метод по созданию настройки мини карточки по пользователю
    *@return bool
    */
    public static function create(int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			$getSettingFieldPortalCollections = SettingsFieldsPortalActions::getSettingFieldPortalCollections($fieldId);

			$settingMinicard =  self::getSettingMiniCardCollection($fieldId);

			if(!is_null($getSettingFieldPortalCollections) && is_null($settingMinicard) && self::getSettingMinicardUserCollections()->count() < 5)
			{	
				DB::commit();

				SettingMinicardUser::create([
					'user_id' => UserActions::getUserId(),
					'settings_fields_portal_id' => $getSettingFieldPortalCollections->id,
					'order' => self::getSettingMinicardUserCollections()->count() + 1 
				]);

				return true;
			}

			return false;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по сортировке настройки мини карточки по пользователю
    *@return bool
    */
    public static function order(int $order, int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			$settingMinicard = self::getSettingMiniCardCollection($fieldId);

			if(!is_null($settingMinicard))
			{	
				DB::commit();

				self::refreshFieldshOrder($settingMinicard->id, $order);

				return true;
			}

			return false;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по обновлению порядка настроек мини карточки по пользователю
    */
    private static function refreshFieldshOrder(int $id, int $changeFieldOrder = 0)
    {
    	DB::beginTransaction();

		try {

			DB::commit();

			if($changeFieldOrder > 0) self::getSettingMinicardUserCollections()->where('id', $id)->update(['order' => $changeFieldOrder]);

			$getSettingMinicardUserCollections = self::getSettingMinicardUserCollections()->where('id' ,'!=', $id)->get();

			$count = $getSettingMinicardUserCollections->count();

			for($i = 0; $i < $count; $i++)
			{	
				$order = $i + 1;

				if($changeFieldOrder > 0)
				{
					if($changeFieldOrder > $order) $order = $i+1;
					elseif($changeFieldOrder <= $order) $order = $i+2;	
				}
				
				$getSettingMinicardUserCollections[$i]->update(['order' => $order]);
			}
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод по удалению порядка настроек мини карточки по пользователю
    */
    public static function delete(int $fieldId): bool
    {
    	DB::beginTransaction();

		try {

			$settingMinicard = self::getSettingMiniCardCollection($fieldId);

			if(!is_null($settingMinicard) && self::getSettingMinicardUserCollections()->count() > 1)
			{	
				DB::commit();

				$settingMinicard->delete();

				self::refreshFieldshOrder($settingMinicard->id, 0);

				return true;
			}

			return false;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
    }

    /**
    *Метод проверяет есть ли настрока по порталу
    *Например если не настроили поле цену то нельзя использовать это поле в настройках по пользователю 
    */
    private static function getSettingMiniCardCollection(int $fieldId)
    {
    	$getSettingFieldPortalCollections = SettingsFieldsPortalActions::getSettingFieldPortalCollections($fieldId);

		$settingMiniCard = (!is_null($getSettingFieldPortalCollections)) ? $getSettingFieldPortalCollections->settingMiniCard : null;

		return $settingMiniCard;
    }
}