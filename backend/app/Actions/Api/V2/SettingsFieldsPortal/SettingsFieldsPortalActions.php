<?php
namespace App\Actions\Api\V2\SettingsFieldsPortal;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Arr;
use App\Actions\Api\V2\User\UserActions;
use App\Actions\Api\V2\Field\FieldActions;
use App\Models\Api\V2\SettingsFieldsPortal;
use App\Repository\Bitrix\BitrixRepository;

/**
*Класс по информации, настройкам полей для портала
*/
class SettingsFieldsPortalActions
{
	/**
    *Получаем настройки полей по порталу
    *@return array
    */
	public static function getSettingsFieldsPortal(): array
	{
		try {

			$arrResult = [];

			$fields = FieldActions::getFieldsCollections();

			for($iField = 0; $iField < $fields->count(); $iField++)
			{	
				$keyField = $fields[$iField];

				$arrResult[$iField] = [
					'id' => $keyField->id,
					'name' => $keyField->name,
					'const' => $keyField->const,
					'relationships' => [
						'bxFields' => BitrixRepository::getCrmProductFields($keyField->typesField->pluck('const')->toArray()),
					]
				];

				$getSettingFieldPortal = [];

				$getSettingFieldPortal = self::getSettingFieldPortal(intval($keyField->id));

				if(!empty($getSettingFieldPortal)) $arrResult[$iField]['relationships']['settingFieldPortal'] = $getSettingFieldPortal; 
			}

			return $arrResult;
			
		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Получаем настройку поля по порталу
    *@param int $fieldId
    *@return array
    */
	public static function getSettingFieldPortal(int $fieldId): array
	{	
		try {
			
			$settingsFieldsPortal = self::getSettingFieldPortalCollections($fieldId);

			if(!is_null($settingsFieldsPortal))
			{	
				$bxFieldConst = $settingsFieldsPortal->bx_field_const;

				$bxField = Arr::first(BitrixRepository::getCrmProductFields([]), function ($value, $key) use($bxFieldConst) {
                    return $value['const'] == $bxFieldConst;
                });
				
				return array_merge($settingsFieldsPortal->toArray(), ['relationships' => ['bxField' => $bxField]]);
			}

			return [];

		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Возращаем коллекцию по настройке портала
    *@param int $fieldId
    *@return use App\Models\Api\V2\SettingsFieldsPortal;
    */
	public static function getSettingFieldPortalCollections(int $fieldId)
	{
		try {
			return SettingsFieldsPortal::where(['portal_id' => UserActions::getUserPortalId(), 'field_id' => $fieldId])->first();
		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Возращаем коллекцию по настройкам портала
    *@return use App\Models\Api\V2\SettingsFieldsPortal;
    */
	public static function getSettingFieldsPortalCollections()
	{
		try {
			return SettingsFieldsPortal::where(['portal_id' => UserActions::getUserPortalId()]);
		} catch (Exception $e) {
			abort(500);
		}
	}

	/**
    *Проверяем наличие настройки по порталу
    *@return bool
    */
	public static function checkSettings(): bool
    {
        try {
        	
            return (self::getSettingFieldsPortalCollections()->count() > 0);

        } catch (Exception $e) {
            abort(500); 
        }
    }

    /**
    *Создаем по умолчанию настройки поля по порталу
    *@return void
    */
	public static function createSettings(): void
	{
		DB::beginTransaction();

		try {

			$fields = FieldActions::getFieldsCollections();

			for($i = 0; $i < $fields->count(); $i++)
			{
				if(in_array($fields[$i]['const'], ['name', 'price', 'description', 'images']))
				{
					$bxFieldConst = '';

					if($fields[$i]['const'] == 'name') $bxFieldConst = 'NAME';

					if($fields[$i]['const'] == 'price') $bxFieldConst = 'PRICE';

					if($fields[$i]['const'] == 'description') $bxFieldConst = 'DESCRIPTION';

					if($fields[$i]['const'] == 'images') $bxFieldConst = 'DETAIL_PICTURE';

					if(empty($bxFieldConst)) continue;

					self::updateOrCreate($bxFieldConst, $fields[$i]->id);
				}
			}

			DB::commit();
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
	}

	/**
    *Создаем или редактируем настройку поля по порталу
    *@param string $bxFieldConst(поле из битрикса назвнание)
    *@param int $fieldId
    *@return bool
    */
	public static function updateOrCreate(string $bxFieldConst, int $fieldId): int
	{
		DB::beginTransaction();

		try {

			$SettingsFieldsPortal = SettingsFieldsPortal::updateOrCreate(
                [   
                    'field_id' => $fieldId, 
                    'portal_id' => UserActions::getUserPortalId()
                ],
                [
                    'bx_field_const' => $bxFieldConst
                ]
            );

			DB::commit();

			return $SettingsFieldsPortal->id;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
	}

	/**
    *Удаляем настройку поля по порталу
    *@param int $fieldId
    *@return bool
    */
	public static function destroy(int $fieldId): bool
	{
		DB::beginTransaction();

		try {

			$getSettingFieldPortalCollections = self::getSettingFieldPortalCollections($fieldId);

			$getSettingFieldsPortalCollections = self::getSettingFieldsPortalCollections();

			if($getSettingFieldsPortalCollections->count() > 1 && !is_null($getSettingFieldPortalCollections))  
			{
				$getSettingFieldPortalCollections->delete();

				DB::commit();
				
				return true;
			}

			return false;
		
		} catch (Exception $e) {
			DB::rollBack();
			abort(500);
		}
	}
}