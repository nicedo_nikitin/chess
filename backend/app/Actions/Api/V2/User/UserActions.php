<?php
namespace App\Actions\Api\V2\User;
use App\Models\Api\V2\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Crypt;
use App\Actions\Api\V2\Portal\PortalActions;
use App\Actions\Api\V2\Module\ModuleActions;
use App\Actions\Api\V2\SettingDetailcardUser\SettingDetailcardUserActions;
use App\Actions\Api\V2\SettingFilterUser\SettingFilterUserActions;
use App\Actions\Api\V2\SettingMinicardUser\SettingMinicardUserActions;

/**
*Класс по информации пользователя
*/
class UserActions
{

    private static $getUser;

    /**
    *Возращаем id пользователя
    *@return int
    */
	public static function getUserId(): int
	{
        try {
            return intval(self::getUser()['id']);
        } catch (Exception $e) {
            abort(500);
        }
	}

    /**
    *Возращаем id портала пользователя
    *@return int
    */
    public static function getUserPortalId():int
    {
        try {
            return self::getUser()['portal_id'];
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
    *Информация по пользователю
    *@return array
    */
    public static function getUser(): array
    {   
        try {

            if(is_null(self::$getUser))
            {
                self::$getUser = [];

                $user = auth()->user()->toArray();

                foreach ($user as $key => $value) {
                    if(in_array($key, ['refresh_token', 'access_token']))  self::$getUser[$key] = Crypt::decryptString($value);
                    else self::$getUser[$key] = $value;
                }

                self::$getUser['relationships']['portal'] = PortalActions::getPortal($user['portal_id']);
                
                return self::$getUser;
            }
            else
            {
                return self::$getUser;
            }
            
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
    *Создаем или редактируем пользователя
    *Поля refresh_token, access_token' шифруем для бд
    *@param int $bxId (Id пользователя в Битрикс24)
    *@param string $memberId
    *@param string $accessToken
    *@param string $refreshToken
    *@return int
    */
	public static function updateOrCreate(int $bxId, string $memberId, string $accessToken, string $refreshToken): int
    {
        DB::beginTransaction();

        try{
            
            $user = User::updateOrCreate(
                [   
                    'portal_id' => PortalActions::getPortalId($memberId), 
                    'bx_id' => $bxId,
                    'module_id' => ModuleActions::getModuleId()
                ],
                [
                    'refresh_token' => Crypt::encryptString($refreshToken),
                    'access_token' => Crypt::encryptString($accessToken),
                ]
            );

            DB::commit();

            return $user->id;

        }catch(\Exception $ex){
            DB::rollBack();
            abort(500);
        }
    }

    /**
    *Метод обновляет JWT токен и возращает его
    */
    public static function refresh()
    {
        try {
            return auth()->refresh();
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
    *Метод проверяет есть ли дефолтные настройки у пользователя
    *Если нет создает
    */
    public function createSettings()
    {
        DB::beginTransaction();

        try {

            SettingDetailcardUserActions::createSettings(); 

            SettingFilterUserActions::createSettings();

            SettingMinicardUserActions::createSettings();

            DB::commit();

        } catch (\Exception $e) {
            abort(500);
        }
    }
}