<?php
namespace App\Http\Controllers\Api\V2;
use App\Http\Controllers\Controller;
use App\Actions\Api\V2\Filter\FilterActions;
use App\Http\Resources\Api\V2\Filter\FilterIndexCollection;

class FilterController extends MainController
{
	  /**
    *Запрос по выводу фильтра в зависимости от настроек
    *@api
    *@return use App\Http\Resources\Api\V2\Filter\FilterIndexCollection
    */
    public function index()
    {
        try {
          	return new FilterIndexCollection([FilterActions::getFilter()]);
       	}catch (Exception $e) {
          abort(500);
       	}
    }
}
