<?php
namespace App\Http\Controllers\Api\V2;
use App\Models\Api\V1\Product;
use Illuminate\Http\Request;
use App\Http\Resources\Api\V2\Product\ProductIndexCollection;
use App\Http\Resources\Api\V2\Product\ProductShowCollection;
use App\Actions\Api\V2\Product\ProductActions;

class ProductController extends MainController
{   
    /**
    *Запрос по выводу товаров из битрикса 24
    *@api
    *@param use Illuminate\Http\Request $request
    *@param int $page
    *@return use App\Http\Resources\Api\V2\Product\ProductIndexCollection
    */
    public function index(Request $request, int $page)
    {
        try {
            return new ProductIndexCollection([ProductActions::index($request->all(), $page)]);
        } catch (Exception $e) {
            abort(500);  
        }
    }

    /**
    *Запрос по выводу товароа из битрикса 24
    *@api
    *@param int $id
    *@return use App\Http\Resources\Api\V2\Product\ProductShowCollection
    */
    public function show(int $id)
    {
        try {
          return new ProductShowCollection([ProductActions::show($id)]);
        } catch (Exception $e) {
            abort(500);  
        }
    }
}
