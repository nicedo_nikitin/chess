<?php
namespace App\Http\Controllers\Api\V2;
use Illuminate\Http\Request;
use App\Repository\Bitrix\BitrixRepository;
use App\Http\Resources\Api\V2\Section\SectionShowCollection;


class SectionController extends MainController
{
    /**
    *Запрос по выводу секции товаров из битрикса 24
    *@api
    *@param int $sectionId
    *@return use App\Http\Resources\Api\V2\Section\SectionShowCollection
    */
    public function show(int $sectionId)
    {
        try {
            return new SectionShowCollection([BitrixRepository::getSections($sectionId)]);
        } catch (Exception $e) {
            abort(500);
        }
    }
}
