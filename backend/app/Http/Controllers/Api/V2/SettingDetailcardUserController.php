<?php
namespace App\Http\Controllers\Api\V2;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Api\V2\Field;
use App\Http\Requests\Api\V2\SettingDetailcardUser\DeleteOrCreateRequest;
use App\Actions\Api\V2\SettingDetailcardUser\SettingDetailcardUserActions;
use App\Http\Resources\Api\V2\SettingDetailcardUser\SettingDetailcardUserIndexCollection;
use App\Http\Resources\Api\V2\SettingDetailcardUser\SettingDetailcardUserDeleteOrCreateCollection;
use App\Http\Resources\Api\V2\SettingDetailcardUser\SettingDetailcardUserOrderCollection;
use App\Http\Resources\Api\V2\Error\Change\ErrorChangeCollection;

class SettingDetailcardUserController extends Controller
{
    /**
    *Запрос по выводу возможных детальной настроки юзера и уже существующих настроек
    *@api
    *@return use App\Http\Resources\Api\V2\SettingDetailcardUser\SettingDetailcardUserIndexCollection
    */
    public function index()
    {
        try {
          return new SettingDetailcardUserIndexCollection([SettingDetailcardUserActions::getSettingDetailcardUser()]);
       } catch (Exception $e) {
          abort(500);
       }
    }

    /**
    *Запрос по удалению детальной настроки юзера
    *@api
    *@param use App\Http\Requests\Api\V2\SettingDetailcardUser\DeleteOrCreateRequest
    *@param use App\Models\Api\V2\Field;
    *@return use App\Http\Resources\Api\V2\SettingDetailcardUser\SettingDetailcardUserDeleteOrCreateCollection;
    */
    public function deleteOrCreate(DeleteOrCreateRequest $request, Field $field)
    {
        DB::beginTransaction();

        try {

            $active = ($request->input('active') == 'true') ? true : false;

            $deleteOrCreate = SettingDetailcardUserActions::deleteOrCreate($active, $field->toArray()['id']);

            DB::commit();

            if($deleteOrCreate) return new SettingDetailcardUserDeleteOrCreateCollection([]);
            
            return new ErrorChangeCollection([]); 
            
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }   
    }

    /**
    *Запрос по сортировке детальной настроки юзера
    *@api
    *@param use App\Models\Api\V2\Field;
    *@param int order
    *@return use App\Http\Resources\Api\V2\SettingDetailcardUser\SettingDetailcardUserOrderCollection;
    */
    public function order(Field $field, int $order)
    {
        DB::beginTransaction();

        try {
           
            DB::commit();

            $order = SettingDetailcardUserActions::order($order, $field->toArray()['id']);

            if($order) return new SettingDetailcardUserOrderCollection([]);

            return new ErrorChangeCollection([]); 

        }catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }   
    }
}
