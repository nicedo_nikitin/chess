<?php
namespace App\Http\Controllers\Api\V2;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Api\V2\SettingFilterUser\DeleteOrCreateRequest;
use App\Models\Api\V2\Field;
use App\Actions\Api\V2\SettingFilterUser\SettingFilterUserActions;
use App\Http\Resources\Api\V2\SettingsFilterUser\SettingsFilterUserIndexCollection;
use App\Http\Resources\Api\V2\SettingsFilterUser\SettingsFilterUserDeleteOrCreateCollection;
use App\Http\Resources\Api\V2\Error\Change\ErrorChangeCollection;

class SettingFilterUserController extends Controller
{
    /**
    *Запрос по выводу возможных настроки фильтра юзера и уже существующих настроек
    *@api
    *@return use App\Http\Resources\Api\V2\SettingsFilterUser\SettingsFilterUserIndexCollection;
    */
    public function index()
    {
        try {
          return new SettingsFilterUserIndexCollection([SettingFilterUserActions::getSettingFilterUser()]);
        } catch (Exception $e) {
          abort(500);
        }
    }

    /**
    *Запрос по удалению настроки фильтра юзера
    *@api
    *@param use App\Http\Requests\Api\V2\SettingFilterUser\DeleteOrCreateRequest;
    *@param use App\Models\Api\V2\Field;
    *@return use App\Http\Resources\Api\V2\SettingsFilterUser\SettingsFilterUserDeleteOrCreateCollection
    */
    public function deleteOrCreate(DeleteOrCreateRequest $request, Field $field)
    {
        DB::beginTransaction();

        try {
        	
            $active = ($request->input('active') == 'true') ? true : false;

            $deleteOrCreate = SettingFilterUserActions::deleteOrCreate($active, $field->toArray()['id']);

            DB::commit();

            if($deleteOrCreate) return new SettingsFilterUserDeleteOrCreateCollection([]);
            
            return new ErrorChangeCollection([]); 
            
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }   
    }
}
