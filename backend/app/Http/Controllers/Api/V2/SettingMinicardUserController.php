<?php
namespace App\Http\Controllers\Api\V2;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Api\V2\Field;
use App\Http\Requests\Api\V2\SettingMinicardUser\DeleteOrCreateRequest;
use App\Actions\Api\V2\SettingMinicardUser\SettingMinicardUserActions;
use App\Http\Resources\Api\V2\SettingsMinicardUser\SettingsMinicardUserIndexCollection;
use App\Http\Resources\Api\V2\SettingsMinicardUser\SettingsMinicardUserDeleteOrCreateCollection;
use App\Http\Resources\Api\V2\SettingsMinicardUser\SettingsMinicardUserOrderCollection;
use App\Http\Resources\Api\V2\Error\Change\ErrorChangeCollection;

class SettingMinicardUserController extends Controller
{
    /**
    *Запрос по выводу возможных настроки миникарточки юзера и уже существующих настроек;
    *@api
    *@return use App\Http\Resources\Api\V2\SettingsMinicardUser\SettingsMinicardUserIndexCollection;
    */
    public function index()
    {
       try {
            return new SettingsMinicardUserIndexCollection([SettingMinicardUserActions::getSettingMinicardUser()]);
       } catch (Exception $e) {
           abort(500);
       }
    }

    /**
    *Запрос по удалению настроки миникарточки юзера;
    *@api
    *@param use App\Http\Requests\Api\V2\SettingMinicardUser\DeleteOrCreateRequest;
    *@param use App\Models\Api\V2\Field;
    *@return use App\Http\Resources\Api\V2\SettingsFilterUser\SettingsFilterUserDeleteOrCreateCollection;
    */
    public function deleteOrCreate(DeleteOrCreateRequest $request, Field $field)
    {
        DB::beginTransaction();

        try {

            $active = ($request->input('active') == 'true') ? true : false;

            $deleteOrCreate = SettingMinicardUserActions::deleteOrCreate($active, $field->toArray()['id']);

            DB::commit();

            if($deleteOrCreate) return new SettingsMinicardUserDeleteOrCreateCollection([]);
            
            return new ErrorChangeCollection([]); 
            
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }   
    }

    /**
    *Запрос по сортировке настроки миникарточки юзера;
    *@api
    *@param use App\Models\Api\V2\Field;
    *@param int $order;
    *@return use App\Http\Resources\Api\V2\SettingsFilterUser\SettingsFilterUserDeleteOrCreateCollection;
    */
    public function order(Field $field, int $order)
    {
        DB::beginTransaction();

        try {
           
            DB::commit();

            $order = SettingMinicardUserActions::order($order, $field->toArray()['id']);

            if($order) return new SettingsMinicardUserOrderCollection([]);

            return new ErrorChangeCollection([]); 

        }catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }   
    }
}
