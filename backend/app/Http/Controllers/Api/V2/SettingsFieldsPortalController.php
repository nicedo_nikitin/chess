<?php
namespace App\Http\Controllers\Api\V2;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Actions\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalActions;
use App\Http\Resources\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalIndexCollection;
use App\Http\Resources\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalUpdateOrCreateCollection;
use App\Http\Resources\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalDestroyCollection;
use App\Http\Resources\Api\V2\Error\Delete\ErrorDeleteCollection;
use App\Http\Requests\Api\V2\SettingsFieldsPortal\UpdateOrCreateRequest;
use App\Models\Api\V2\Field;

class SettingsFieldsPortalController extends Controller
{
    /**
    *Запрос по выводу возможных настроки полей портала и уже существующих настроек
    *Может только юзер с админскими правами
    *@api
    *@return use App\Http\Resources\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalIndexCollection;
    */
    public function index()
    {
        try {
            return new SettingsFieldsPortalIndexCollection([SettingsFieldsPortalActions::getSettingsFieldsPortal()]);
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
    *Запрос по созданию или редактированию настроки полей портала
    *Может только юзер с админскими правами
    *@api
    *@param use App\Http\Requests\Api\V2\SettingsFieldsPortal\UpdateOrCreateRequest;
    *@param use App\Models\Api\V2\Field;
    *@return use App\Http\Resources\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalIndexCollection;
    */
    public function updateOrCreate(UpdateOrCreateRequest $request, Field $field)
    {
        DB::beginTransaction();
        try {
            
            SettingsFieldsPortalActions::updateOrCreate($request->input('bxFieldConst'), $field->toArray()['id']);

            DB::commit();

            return new SettingsFieldsPortalUpdateOrCreateCollection([]);

        }catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
    }

    /**
    *Запрос по удалению настроки полей портала
    *Может только юзер с админскими правами
    *@api
    *@param use App\Models\Api\V2\Field;
    *@return use App\Http\Resources\Api\V2\SettingsFieldsPortal\SettingsFieldsPortalDestroyCollection;
    */
    public function destroy(Field $field)
    {
        DB::beginTransaction();
        try {

            $SettingsFieldsPortalActions = SettingsFieldsPortalActions::destroy($field->toArray()['id']);

            DB::commit();

            if($SettingsFieldsPortalActions) return new SettingsFieldsPortalDestroyCollection([]);
            
            return new ErrorDeleteCollection([]);

        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
    }
}
