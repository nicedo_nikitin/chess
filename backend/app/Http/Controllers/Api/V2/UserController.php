<?php
namespace App\Http\Controllers\Api\V2;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\Api\V2\Bitrix\AuthBitrixRequest;
use App\Models\Api\V2\User;
use App\Actions\Api\V2\Portal\PortalActions;
use App\Actions\Api\V2\User\UserActions;
use App\Repository\Bitrix\BitrixRepository;
use App\Repository\Consts\ConstsRepository;
use App\Http\Resources\Api\V2\User\UserShowCollection;
use App\Http\Resources\Api\V2\User\UserRefreshCollection;

/**
*Класс для работы с пользователем по порталу
*@api
*/
class UserController extends MainController
{   
    /**
    *Запрос по выводу информации пользователя
    *@api
    *@return use App\Http\Resources\Api\V2\User\UserShowCollection;
    */
    public function show()
    {
        try {
            return new UserShowCollection([UserActions::getUser()]);
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
    *Запрос при входе через приложение в Битриксе24
    *Создается новый портал и пользователь если нет записи в бд
    *redirect на front withCookie(AUTH_BITRIX_COOKIE)
    *@api
    *@return use App\Http\Resources\Api\V2\User\UserShowCollection;
    */
    public function auth(AuthBitrixRequest $request)
    {   
        DB::beginTransaction();

        try {

            $requestArray = $request->toArray();

            $portalId = PortalActions::updateOrCreate(
                $requestArray[ConstsRepository::getKeysBitrixAuth()['member_id']], 
                $requestArray[ConstsRepository::getKeysBitrixAuth()['DOMAIN']]
            );

            $userId = UserActions::updateOrCreate(
                BitrixRepository::getProfileId(), 
                $requestArray[ConstsRepository::getKeysBitrixAuth()['member_id']], 
                $requestArray[ConstsRepository::getKeysBitrixAuth()['AUTH_ID']], 
                $requestArray[ConstsRepository::getKeysBitrixAuth()['REFRESH_ID']]
            );

            if ($token = auth()->fromUser(User::find($userId))) {

                $cookie = cookie(env('AUTH_BITRIX_COOKIE'), $token);

                DB::commit();

                $url = 'https://broking.su/apps/chessv3/frontend/bitrix/realestate/';

                return redirect($url)->withCookie($cookie);
            }
            
        } catch (Exception $e) {
            DB::rollBack();
            abort(500);
        }
    }

    /**
    *Refresh jwt token
    *Action token 120 min
    *@api
    *@return use App\Http\Resources\Api\V2\User\UserRefreshCollection;
    */
    public function refresh()
    {
        try {
            return new UserRefreshCollection([]);
        } catch (Exception $e) {
           abort(500); 
        }
    }
}
