<?php
namespace App\Http\Controllers\Web\V2;
use App\Http\Controllers\Controller;

class BitrixController extends Controller
{
	/**
    *Установка приложения битрикс.
    *@api
    */
    public function install()
    {
        return view('bitrix.install');
    }
}
