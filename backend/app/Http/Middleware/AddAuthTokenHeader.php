<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AddAuthTokenHeader
{
    public function handle($request, Closure $next)
    {
        $cookie_name = env('AUTH_BITRIX_COOKIE');

        if (!$request->bearerToken() && $request->hasCookie($cookie_name)){
            
            $token = $request->cookie($cookie_name);

            $request->headers->add([
                'Authorization' => 'Bearer ' . $token
            ]); 
        }
        
        return $next($request);
    }
}
