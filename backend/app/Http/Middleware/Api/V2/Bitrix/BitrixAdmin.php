<?php

namespace App\Http\Middleware\Api\V2\Bitrix;

use Closure;
use Illuminate\Http\Request;
use App\Http\Resources\Api\V2\Error\Bitrix\ErrorBitrixAdminCollection;
use App\Repository\Bitrix\BitrixRepository;


class BitrixAdmin
{
    public function handle($request, Closure $next)
    {
        if(!BitrixRepository::getProfileAdmin()) return new ErrorBitrixAdminCollection([]);

        return $next($request);
    }
}
