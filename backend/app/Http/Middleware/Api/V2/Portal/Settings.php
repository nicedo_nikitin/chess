<?php
namespace App\Http\Middleware\Api\V2\Portal;
use Closure;
use Illuminate\Http\Request;
use App\Actions\Api\V2\Portal\PortalActions;

class Settings
{
    public function handle($request, Closure $next)
    {
    	PortalActions::createSettings();

        return $next($request);
    }
}
