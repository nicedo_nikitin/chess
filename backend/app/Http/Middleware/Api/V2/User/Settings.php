<?php
namespace App\Http\Middleware\Api\V2\User;
use Closure;
use Illuminate\Http\Request;
use App\Actions\Api\V2\User\UserActions;

class Settings
{
    public function handle($request, Closure $next)
    {
    	UserActions::createSettings();

        return $next($request);
    }
}
