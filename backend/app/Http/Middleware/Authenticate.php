<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use App\Http\Resources\Api\V2\Error\Auth\ErrorAuthCollection;
use Illuminate\Http\Exceptions\HttpResponseException;

class Authenticate extends Middleware
{
    protected function redirectTo($request)
    {
        if (! $request->expectsJson()) {
	        throw new HttpResponseException(ErrorAuthCollection::responce($request));
        }
    }
}
