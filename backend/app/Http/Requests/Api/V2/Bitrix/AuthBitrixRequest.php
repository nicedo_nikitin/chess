<?php
namespace App\Http\Requests\Api\V2\Bitrix;
use Illuminate\Foundation\Http\FormRequest;
use App\Repository\Consts\ConstsRepository;
use App\Traits\Http\Requests\Api\V2\FailedValidation;

class AuthBitrixRequest extends FormRequest
{	
	use FailedValidation;

    public function authorize()
    {
        return true;
    }

	public function rules(): array
	{
		return [
			ConstsRepository::getKeysBitrixAuth()['AUTH_ID'] => ['required', 'string'],
			ConstsRepository::getKeysBitrixAuth()['REFRESH_ID'] => ['required', 'string'],
			ConstsRepository::getKeysBitrixAuth()['member_id']   => ['required', 'string'],
			ConstsRepository::getKeysBitrixAuth()['DOMAIN'] => ['required', 'string'],
		];
	}
}