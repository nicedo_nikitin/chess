<?php
namespace App\Http\Requests\Api\V2\SettingDetailcardUser;
use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Http\Requests\Api\V2\FailedValidation;

class DeleteOrCreateRequest extends FormRequest
{	
	use FailedValidation;

    public function authorize()
    {
        return true;
    }

	public function rules(): array
	{
		return [
			'active' => ['required', 'boolean']
		];
	}
}