<?php
namespace App\Http\Requests\Api\V2\SettingsFieldsPortal;
use Illuminate\Foundation\Http\FormRequest;
use App\Traits\Http\Requests\Api\V2\FailedValidation;

class UpdateOrCreateRequest extends FormRequest
{	
	use FailedValidation;

    public function authorize()
    {
        return true;
    }

	public function rules(): array
	{
		return [
			'bxFieldConst' => ['required', 'string']
		];
	}
}