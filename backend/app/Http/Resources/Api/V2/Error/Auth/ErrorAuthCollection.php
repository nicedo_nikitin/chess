<?php

namespace App\Http\Resources\Api\V2\Error\Auth;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\ErrorResponse;

class ErrorAuthCollection extends ResourceCollection
{
    use ErrorResponse;

    public static function getStatus(): string
    {
        return 'unauthorized';
    }

    public static function getInfo(): array
    {
        return ['Авторизация истекла!'];
    }

    public static function getStatusCode():int
    {
        return 401;
    }
}
