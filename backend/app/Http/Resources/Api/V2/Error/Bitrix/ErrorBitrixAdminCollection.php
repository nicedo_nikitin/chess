<?php

namespace App\Http\Resources\Api\V2\Error\Bitrix;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\ErrorResponse;

class ErrorBitrixAdminCollection extends ResourceCollection
{
    use ErrorResponse;

    public static function getStatus(): string
    {
        return 'forbidden';
    }

    public static function getInfo(): array
    {
        return ['Доступ только для админов Битрикс24!'];
    }

    public static function getStatusCode():int
    {
        return 403;
    }
}
