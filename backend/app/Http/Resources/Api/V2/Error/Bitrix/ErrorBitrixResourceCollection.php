<?php

namespace App\Http\Resources\Api\V2\Error\Bitrix;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\ErrorResponse;

class ErrorBitrixResourceCollection extends ResourceCollection
{
    use ErrorResponse;
}
