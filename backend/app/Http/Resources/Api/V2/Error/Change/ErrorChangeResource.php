<?php

namespace App\Http\Resources\Api\V2\Error\Change;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Repository\Consts\ConstsRepository;

class ErrorChangeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [];
    }
}