<?php

namespace App\Http\Resources\Api\V2\Error\Delete;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\ErrorResponse;

class ErrorDeleteCollection extends ResourceCollection
{
    use ErrorResponse;

    public static function getStatus(): string
    {
        return 'badRequest';
    }

    public static function getInfo(): array
    {
        return ['Запись не может быть удалена!'];
    }

    public static function getStatusCode():int
    {
        return 400;
    }
}
