<?php

namespace App\Http\Resources\Api\V2\Error\Validate;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\ErrorResponse;

class ErrorValidateCollection extends ResourceCollection
{
    use ErrorResponse;

    private static $getinfo = [];

    public static function getStatus(): string
    {
        return 'badRequest';
    }

    public static function setInfo(array $array): void
    {
        self::$getinfo = $array;
    }

    public static function getInfo(): array
    {
        return self::$getinfo;
    }

    public static function getStatusCode():int
    {
        return 400;
    }
}
