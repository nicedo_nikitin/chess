<?php

namespace App\Http\Resources\Api\V2\Error\Delete;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Repository\Consts\ConstsRepository;

class ErrorValidateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [];
    }
}