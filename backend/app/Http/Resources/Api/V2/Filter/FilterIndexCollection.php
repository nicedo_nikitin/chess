<?php

namespace App\Http\Resources\Api\V2\Filter;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class FilterIndexCollection extends ResourceCollection
{
    use SuccessResponse;
}
