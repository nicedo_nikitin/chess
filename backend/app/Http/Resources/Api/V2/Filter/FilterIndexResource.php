<?php
namespace App\Http\Resources\Api\V2\Filter;
use Illuminate\Http\Resources\Json\JsonResource;

class FilterIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   
        $arrResult = [];

        if(isset($this->resource) && !empty($this->resource))
        {
            $result = $this->resource;

            foreach ($result as $key => $value) {
                
                $arrResult[$key]['type'] = 'filter';

                $arrResult[$key]['attributes'] = [
                    'name' => $value['name'],
                    'const' => $value['const'],
                    'type' => $value['type']
                ];  
            }
        }

        return $arrResult;
    }
}