<?php
namespace App\Http\Resources\Api\V2\Product;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Repository\Consts\ConstsRepository;
use Illuminate\Support\Arr;

class ProductIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $arrResult = [];

        if(isset($this->resource['result']) && !empty($this->resource['result']))
        {
            $result = $this->resource['result'];

            for($i=0; $i < count($result); $i++)
            {   
                $productId = intval(implode('', $result[$i]['id']['values']));

                $arrResult[$i]['id'] = $productId;

                $arrResult[$i]['type'] = 'products';

                $arrResult[$i]['attributes'] = [];

                foreach ($result[$i] as $key => $value) {

                    if(in_array($key, ['id', 'sectionId', 'catalogId'])) continue;

                    $values = [];
                   
                    foreach ($value['values'] as $stringValue) {
                        if(strlen($stringValue) >= 100) $values[] = mb_strimwidth($stringValue, 0, 97, "..."); 
                        else $values[] = $stringValue;
                    }    

                    $arrResult[$i]['attributes'][$key] = Arr::only($value, ['name', 'const', 'order']); 

                    $arrResult[$i]['attributes'][$key]['values'] =  $values;   
                }

                $arrResult[$i]['attributes'] = array_values(Arr::sort($arrResult[$i]['attributes'], function($value)
                {
                  return (isset($value['order'])) ? $value['order'] : 123456;
                }));
            }
        }

        return $arrResult;
    }
}