<?php

namespace App\Http\Resources\Api\V2\Product;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class ProductShowCollection extends ResourceCollection
{
    use SuccessResponse;
}
