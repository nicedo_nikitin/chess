<?php
namespace App\Http\Resources\Api\V2\Product;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use App\Repository\Bitrix\BitrixRepository;

class ProductShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $arrResult = [];

        $result = $this->resource;

        $productId = intval(implode('', $result['id']['values']));

        $catalogId = intval(implode('', $result['catalogId']['values']));

        $arrResult['id'] = $productId;

        $arrResult['type'] = 'products';

        $arrResult['attributes'] = [];

        foreach ($result as $key => $value) {
            if(!in_array($key, ['id', 'sectionId', 'catalogId'])) $arrResult['attributes'][$key] = Arr::only($value, ['name', 'const', 'order', 'values']); 
        }

        $arrResult['attributes'] = array_values(Arr::sort($arrResult['attributes'], function($value)
        {
          return (isset($value['order'])) ? $value['order'] : 123456;
        }));
        
        $arrResult['links'] = [
            'product' => BitrixRepository::getLinkProduct($catalogId, $productId),
            'createDeal' => BitrixRepository::getLinkCreateDeal()
        ];
    
        return $arrResult;
    }
    
}