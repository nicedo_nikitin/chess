<?php

namespace App\Http\Resources\Api\V2\Section;

use Illuminate\Http\Resources\Json\ResourceCollection;
use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class SectionShowCollection extends ResourceCollection
{
    use SuccessResponse;
}
