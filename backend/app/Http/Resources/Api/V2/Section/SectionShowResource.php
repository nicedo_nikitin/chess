<?php
namespace App\Http\Resources\Api\V2\Section;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Repository\Consts\ConstsRepository;

class SectionShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   
        $arrResult = [];

        if(isset($this->resource['result']) && !empty($this->resource['result']))
        {
            $result = $this->resource['result'];

            foreach ($result as $key => $value) {
                $arrResult[] = [
                    'id' => intval($value[ConstsRepository::getKeysBitrixSections()['ID']]),
                    'type' => 'sections',
                    'attributes' => [
                        'sectionId' => (intval($value[ConstsRepository::getKeysBitrixSections()["SECTION_ID"]]) > 0) ? $value[ConstsRepository::getKeysBitrixSections()["SECTION_ID"]] : 0,
                        'name' => $value[ConstsRepository::getKeysBitrixSections()["NAME"]],
                    ]
                ];
            }
        }
        
        return $arrResult;
    }
}
