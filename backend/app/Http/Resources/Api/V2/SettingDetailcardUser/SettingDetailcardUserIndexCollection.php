<?php

namespace App\Http\Resources\Api\V2\SettingDetailcardUser;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class SettingDetailcardUserIndexCollection extends ResourceCollection
{
    use SuccessResponse;
}
