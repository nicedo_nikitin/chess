<?php
namespace App\Http\Resources\Api\V2\SettingDetailcardUser;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;

class SettingDetailcardUserIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   
        $arrResult = [];

        if(isset($this->resource) && !empty($this->resource))
        {
            $result = $this->resource;

            foreach($result as $key => $value){
                
                $arrResult[$key] = [
                    'type' => 'detailcard',
                ];

                $arrResult[$key]['attributes'] = [
                    'fieldId' => $value['relationships']['field']['id'],
                    'active' => $value['active'],
                ];

                if(isset($value['order']))  $arrResult[$key]['attributes']['order'] = $value['order'];
                    
                $arrResult[$key]['relationships']['field'] = [
                    'id' => $value['relationships']['field']['id'],
                    'name' => $value['relationships']['field']['name'],
                    'const' => $value['relationships']['field']['const'],
                    'type' => 'field',

                ];   
            }

            $arrResult = array_values(Arr::sort($arrResult, function($value)
            {
                return (isset($value['attributes']['order'])) ? $value['attributes']['order'] : 123456;
            }));
        }

        return $arrResult;
    }
}