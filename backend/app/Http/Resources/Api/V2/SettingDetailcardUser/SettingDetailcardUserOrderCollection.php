<?php

namespace App\Http\Resources\Api\V2\SettingDetailcardUser;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class SettingDetailcardUserOrderCollection extends ResourceCollection
{
    use SuccessResponse;

    public static function getInfo(): array
    {
    	return ['Запись успешно изменена!'];
    }
}
