<?php

namespace App\Http\Resources\Api\V2\SettingsFieldsPortal;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class SettingsFieldsPortalDestroyCollection extends ResourceCollection
{
    use SuccessResponse;

    public static function getInfo(): array
    {
    	return ['Запись успешно удалена!'];
    }

    public static function getStatus(): string
    {
        return 'noContent';
    }

    public static function getStatusCode():int
    {
        return 200;
    }
}
