<?php

namespace App\Http\Resources\Api\V2\SettingsFieldsPortal;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class SettingsFieldsPortalIndexCollection extends ResourceCollection
{
    use SuccessResponse;
}
