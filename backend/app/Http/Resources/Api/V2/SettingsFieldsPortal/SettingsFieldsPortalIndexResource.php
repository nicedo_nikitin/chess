<?php

namespace App\Http\Resources\Api\V2\SettingsFieldsPortal;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Repository\Consts\ConstsRepository;

class SettingsFieldsPortalIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   
        $arrResult = [];

        if(isset($this->resource) && !empty($this->resource))
        {
            $result = $this->resource;

            foreach ($result as $key => $value) {
                
                $arrResult[$key] = [
                    'type' => 'fields',
                    'id' => $value['id'],
                ];

                $arrResult[$key]['attributes'] = [
                    'name' => $value['name'],
                    'const' => $value['const']
                ];

                if(isset($value['relationships']['settingFieldPortal']['bx_field_const']))
                {
                    $arrResult[$key]['attributes']['bxFieldConst'] = $value['relationships']['settingFieldPortal']['bx_field_const'];
                }

                foreach ($value['relationships']['bxFields'] as $bxFieldsValue) {
                    
                    $arrResult[$key]['relationships']['bxFields'][] = [
                        'bxFieldName' => $bxFieldsValue['title'],
                        'bxFieldConst' => $bxFieldsValue['const'],
                        'selected' => (isset($value['relationships']['settingFieldPortal']['bx_field_const']) && $value['relationships']['settingFieldPortal']['bx_field_const'] == $bxFieldsValue['const']) ? true : false
                    ];

                }   
            }
        }

        return $arrResult;
    }
}