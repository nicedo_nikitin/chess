<?php

namespace App\Http\Resources\Api\V2\SettingsFilterUser;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class SettingsFilterUserDeleteOrCreateCollection extends ResourceCollection
{
    use SuccessResponse;

    public static function getInfo(): array
    {
    	return ['Запись успешно изменена!'];
    }
}
