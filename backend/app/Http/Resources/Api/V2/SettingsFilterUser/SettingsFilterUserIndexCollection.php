<?php

namespace App\Http\Resources\Api\V2\SettingsFilterUser;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class SettingsFilterUserIndexCollection extends ResourceCollection
{
    use SuccessResponse;
}
