<?php

namespace App\Http\Resources\Api\V2\SettingsFilterUser;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Repository\Consts\ConstsRepository;

class SettingsFilterUserIndexResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {   
        $arrResult = [];

        if(isset($this->resource) && !empty($this->resource))
        {
            $result = $this->resource;

            foreach($result as $key => $value){
                
                $arrResult[$key] = [
                    'type' => 'filter',
                ];

                $arrResult[$key]['attributes'] = [
                    'fieldId' => $value['relationships']['field']['id'],
                    'active' => $value['active'],
                ];
                    
                $arrResult[$key]['relationships']['field'] = [
                    'id' => $value['relationships']['field']['id'],
                    'name' => $value['relationships']['field']['name'],
                    'const' => $value['relationships']['field']['const'],
                    'type' => 'field',

                ];   
            }
        }

        return $arrResult;
    }
}