<?php

namespace App\Http\Resources\Api\V2\SettingsMinicardUser;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class SettingsMinicardUserDeleteOrCreateCollection extends ResourceCollection
{
    use SuccessResponse;

    public static function getInfo(): array
    {
    	return ['Запись успешно изменена!'];
    }
}
