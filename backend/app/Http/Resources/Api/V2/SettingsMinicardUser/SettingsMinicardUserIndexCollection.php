<?php

namespace App\Http\Resources\Api\V2\SettingsMinicardUser;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

class SettingsMinicardUserIndexCollection extends ResourceCollection
{
    use SuccessResponse;
}
