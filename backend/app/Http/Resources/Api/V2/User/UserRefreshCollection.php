<?php

namespace App\Http\Resources\Api\V2\User;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

use App\Actions\Api\V2\User\UserActions;

class UserRefreshCollection extends ResourceCollection
{
    use SuccessResponse;

    public function withResponse($request, $response)
    {   
        $response->cookie(env('AUTH_BITRIX_COOKIE'), UserActions::refresh());
    } 
}
