<?php

namespace App\Http\Resources\Api\V2\User;

use Illuminate\Http\Resources\Json\ResourceCollection;

use App\Traits\Http\Resources\Api\V2\SuccessResponse;

use App\Actions\Api\V2\User\UserActions;

class UserShowCollection extends ResourceCollection
{
    use SuccessResponse;
}
