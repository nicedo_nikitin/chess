<?php

namespace App\Http\Resources\Api\V2\User;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Repository\Consts\ConstsRepository;
use App\Repository\Bitrix\BitrixRepository;

class UserShowResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $result = $this->resource;

        $getProfile = BitrixRepository::getProfile()['result'];
        
        return [
            'id' => $result['id'],
            'type' => 'user',
            'attributes' => [
                'admin' => $getProfile[ConstsRepository::getKeysBitrixProfile()['ADMIN']],
                'bxId' => $result['bx_id'],
                'firstName' => $getProfile[ConstsRepository::getKeysBitrixProfile()['NAME']],
                'lastName' => $getProfile[ConstsRepository::getKeysBitrixProfile()['LAST_NAME']],
            ]
        ];
    }
}