<?php

namespace App\Models\Api\V2;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Field extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name', 'const'];

    public function typesField()
    {
      return $this->hasMany(TypeField::class)->select(['types.*'])->leftJoin('types', 'types_fields.type_id', '=', 'types.id');
    }
}
