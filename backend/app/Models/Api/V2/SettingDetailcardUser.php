<?php

namespace App\Models\Api\V2;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingDetailcardUser extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['user_id', 'settings_fields_portal_id', 'order'];

    protected $table = 'settings_detailscards_users';
}
