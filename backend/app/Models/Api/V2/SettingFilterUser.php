<?php

namespace App\Models\Api\V2;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SettingFilterUser extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['user_id', 'settings_fields_portal_id'];

    protected $table = 'settings_filters_users';
}
