<?php

namespace App\Models\Api\V2;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Actions\Api\V2\User\UserActions;

class SettingsFieldsPortal extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['portal_id', 'bx_field_const', 'field_id'];

    public function settingMinicard()
    {
      return $this->hasOne(SettingMinicardUser::class)->where(['user_id' => UserActions::getUserId()]);
    }

    public function settingDetailcard()
    {
      return $this->hasOne(SettingDetailcardUser::class)->where(['user_id' => UserActions::getUserId()]);
    }

    public function settingFilter()
    {
      return $this->hasOne(SettingFilterUser::class)->where(['user_id' => UserActions::getUserId()]);
    }
}
