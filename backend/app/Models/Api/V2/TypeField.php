<?php

namespace App\Models\Api\V2;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TypeField extends Model
{
    use HasFactory;

    protected $table = 'types_fields';
    
    public $timestamps = false;

    protected $fillable = ['type_id', 'field_id'];
}
