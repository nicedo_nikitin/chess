<?php
namespace App\Repository\Bitrix;
use App\Actions\Api\V2\User\UserActions;
use App\Actions\Api\V2\Portal\PortalActions;
use App\Repository\Bitrix\Bx24;
use App\Repository\Consts\ConstsRepository;

class BitrixRepository
{
	private static $getCrmProductFields;

	private static function BX24(): Bx24
	{	
		try {

			$request;

			if(!is_null(request())) $request = request()->toArray();
		
			if(isset( $request[ConstsRepository::getKeysBitrixAuth()["DOMAIN"]],  $request[ConstsRepository::getKeysBitrixAuth()["member_id"]],  $request[ConstsRepository::getKeysBitrixAuth()["REFRESH_ID"]],  $request[ConstsRepository::getKeysBitrixAuth()["AUTH_ID"]]))
			{
			
				$domain = $request[ConstsRepository::getKeysBitrixAuth()["DOMAIN"]];

				$memberId = $request[ConstsRepository::getKeysBitrixAuth()["member_id"]];

				$refreshToken = $request[ConstsRepository::getKeysBitrixAuth()["REFRESH_ID"]];

				$accessToken = $request[ConstsRepository::getKeysBitrixAuth()["AUTH_ID"]];
			}
			else
			{
				$user = UserActions::getUser();

				$domain = $user['relationships']['portal']['domain'];

				$memberId = $user['relationships']['portal']['member_id'];

				$refreshToken = $user['refresh_token'];

				$accessToken = $user['access_token'];
			}

			$clientId = 'app.5fe97f60e83870.82129246';

			$clientSecret = 'FkblagZJiajTkPb5QPcppfZMLbRdkW484NiNNJ0Mj1hTUEFhit';

			$BX24 = new Bx24($domain, $memberId, $refreshToken, $accessToken, $clientId, $clientSecret);

			return $BX24;
			
		} catch (Exception $e) {
			abort(500);
		}
	}

	public static function getLinkProduct(int $catalogId, int $productId): string
	{
		try {
			return self::BX24()->getLinkProduct($catalogId, $productId);
		} catch (Exception $e) {
			abort(500);
		}
	}

	public static function getLinkCreateDeal(): string
	{
		try {
			return self::BX24()->getLinkCreateDeal();
		} catch (Exception $e) {
			abort(500);
		}
	}

	public static function getProfile(): array
	{
		try {
			return self::BX24()->getProfile();
		} catch (Exception $ex) {
			abort(500);
		}
	}

	public static function getProfileId(): int
	{
		try {
			return intval(self::getProfile()['result'][ConstsRepository::getKeysBitrixProfile()['ID']]);
		} catch (Exception $ex) {
			abort(500);
		}
	}

	public static function getProfileAdmin(): bool
	{
		try {
			return self::getProfile()['result'][ConstsRepository::getKeysBitrixProfile()['ADMIN']];
		} catch (Exception $ex) {
			abort(500);
		}
	}

	public static function getSections(int $sectionId): array
	{
		try {
			return self::BX24()->getSections($sectionId);
		} catch (Exception $ex) {
			abort(500);
		}
	}

	public static function getProducts(array $select, array $filter, int $start = 0): array
	{
		try {
			return self::BX24()->getProducts($select, $filter, $start);
		} catch (Exception $ex) {
			abort(500);
		}
	}

	public static function getCrmProductFields(array $types): array
	{
		try {

			if(is_null(self::$getCrmProductFields)) self::$getCrmProductFields = self::BX24()->getCrmProductFields();

			foreach (self::$getCrmProductFields['result'] as $key => $value) {

				if(!isset($value['type'])) continue; 

				$type = $value['type'];

				if($value['type'] == 'product_property' && isset($value['propertyType']) && in_array($value['propertyType'], ['F', 'N', 'S']))
				{
					if($value['propertyType'] == 'N') $type = 'integer';
					if($value['propertyType'] == 'S') $type = 'string';
					if($value['propertyType'] == 'F') $type = 'product_file';
				}

				if(in_array($type, $types) || empty($types))
				{		
					$arrResult[] = [
						'type' => $type,
						'title' => $value['title'],
						'const' => $key,
						'isMultiple' => $value['isMultiple'] 
					];
				}
			}

			return $arrResult;

		} catch (Exception $ex) {
			abort(500);
		}
	}
}