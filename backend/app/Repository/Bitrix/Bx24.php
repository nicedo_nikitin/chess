<?php declare (strict_types = 1);


namespace App\Repository\Bitrix;
use App\Actions\Api\V2\User\UserActions;
use App\Repository\Consts\ConstsRepository;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Resources\Api\V2\Error\Bitrix\ErrorBitrixResourceCollection;
use Illuminate\Support\Facades\Storage;
/**
 * Коды ошибок ответа сервера на REST API запрос:
 * 401 - не авторизирован
 * 403 - портал удален
 * 404 - ошибка в запросе
 * 400 - ошибка в параметрах запроса
 * 500 - сервер недоступен
 * 503 - сервер недоступен
 */

/**
 * Класс выполняет запросы к REST API
 */
class Bx24
{
    use Curl;

    private $instance;

    private $domain;

    private $memberId;

    private $refreshToken;

    private $accessToken;

    private $clientId;

    private $clientSecret;

    private $countRefresh = 0;
    
    public  function __construct(string $domain, string $memberId, string $refreshToken, string $accessToken, string $clientId, string $clientSecret)
    {
        $this->domain = $domain;

        $this->memberId = $memberId;

        $this->refreshToken = $refreshToken;

        $this->accessToken = $accessToken;

        $this->clientId = $clientId;

        $this->clientSecret = $clientSecret;
    }

    private function getDomain(): string
    {
        return $this->domain;
    }

    private function getMemberId(): string
    {
       return $this->memberId; 
    }


    private function getRefreshToken(): string
    {
       return $this->refreshToken; 
    }

    private function getAccessToken(): string
    {
       return $this->accessToken; 
    }

    private function getClientId(): string
    {
       return $this->clientId; 
    }

    private function getClientSecret(): string
    {
       return $this->clientSecret; 
    }

    private function getLinkDownloadUrl(string $path): string
    {
        return 'https://'.$this->getDomain().$path.'&auth='.$this->getAccessToken();
    }

    public function getLinkProduct(int $catalogId, int $productId): string
    {
        try {
            return 'https://'.$this->getDomain().'/crm/catalog/'.$catalogId.'/product/'.$productId.'/';
        } catch (Exception $e) {
            abort(500);
        }
    }

    public function getLinkCreateDeal(): string
    {
        try {
            return 'https://'.$this->getDomain().'/crm/deal/details/0/?category_id=0';
        } catch (Exception $e) {
            abort(500);
        }
    }

    public function getProfile(): array
    {
        try {
            return $this->callMethod('profile', []);
        } catch (Exception $e) {
            abort(500);
        }
    }

    public function getSections(int $sectionId): array
    {
        try {
            return $this->callMethod('crm.productsection.list', 
                [
                    'filter' => [
                        ConstsRepository::getKeysBitrixProducts()['SECTION_ID'] => ($sectionId > 0) ? $sectionId : ''
                    ]
                ]
            );
        } catch (Exception $ex) {
            abort(500);
        }
    }

    public function getProductIdCatalogsPrice(int $priceFrom = 0, int $priceTo = 0): array
    {
        $arrResult = [];

        $filter = [];

        if($priceFrom > 0) $filter['>=price'] = $priceFrom;

        if($priceTo > 0) $filter['<=price'] = $priceTo;

        $getProducts = $this->callMethodAll('catalog.price.list', [
            'filter' => $filter
        ]);

        if(isset($getProducts['response']['result']['prices']) && !empty($getProducts['response']['result']['prices']))
        {
            foreach ($getProducts['response']['result']['prices'] as $key => $value) {
                if(isset($value['productId']) && !empty($value['productId'])) $arrResult[] = $value['productId'];
            }
        }

        return $arrResult;
    }

    public function getProducts(array $select, array $filterBx, int $page = 1): array
    {
        try {

            $start = ($page > 0) ? $page : 1;

            $arrResult = [];

            $selectBx = [
                ConstsRepository::getKeysBitrixProducts()['ID'], 
                ConstsRepository::getKeysBitrixProducts()['CURRENCY_ID'],
                ConstsRepository::getKeysBitrixProducts()['SECTION_ID'],
                ConstsRepository::getKeysBitrixProducts()['CATALOG_ID'] 
            ];

            $selectBx  = array_unique(array_merge($select, $selectBx));

            if(isset($filterBx[ConstsRepository::getKeysBitrixProducts()['SECTION_ID']])) $filterBx[ConstsRepository::getKeysBitrixProducts()['SECTION_ID']] = ($filterBx[ConstsRepository::getKeysBitrixProducts()['SECTION_ID']] > 0) ? $filterBx[ConstsRepository::getKeysBitrixProducts()['SECTION_ID']] : '';

            /**
            *crm.product.list не сортирует по PRICE 
            *если есть цена от или до вызываем catalog.price.list и получаем массив с id товара и передаем в фильтре
            *если массив с id пустой передаем [0] нужен пустой результат
            */

            if(isset($filterBx['>=PRICE']) || isset($filterBx['<=PRICE']))
            {   
                $priceFrom = (isset($filterBx['>=PRICE']) && intval($filterBx['>=PRICE']) > 0) ? intval($filterBx['>=PRICE']) : 0;

                $priceTo = (isset($filterBx['<=PRICE']) && intval($filterBx['<=PRICE']) > 0) ? intval($filterBx['<=PRICE']) : 0;

                if($priceFrom > 0 || $priceTo > 0)
                {
                   $getProductIdCatalogsPrice = $this->getProductIdCatalogsPrice($priceFrom, $priceTo); 

                   $filterBx['ID'] = (!empty($getProductIdCatalogsPrice)) ? $getProductIdCatalogsPrice : [0];
                }
            } 

            $getProducts = $this->callMethod('crm.product.list', [
                'start' => ($start - 1) * 50,
                'filter' => $filterBx,
                'ACTIVE' => 'Y',
                'select' => $selectBx
            ]);
        
            for($i = 0; $i < count($getProducts['result']); $i++)
            {
                foreach($getProducts['result'][$i] as $key => $value)
                {  
                    if(is_array($value))
                    {

                        if(isset($value['value']) && !is_array($value['value']))  $arrResult['result'][$i][$key][] = $value['value'];

                        elseif(isset($value['value']['downloadUrl']) && !empty($value['value']['downloadUrl'])) $arrResult['result'][$i][$key][] = $this->getLinkDownloadUrl($value['value']['downloadUrl']);
                        
                        else
                        {
                            $downloadUrl = data_get($value, '*.value.downloadUrl');

                            foreach ($downloadUrl as $valueDownloadUrl) {
                                if(!empty($valueDownloadUrl)) $arrResult['result'][$i][$key][] = $this->getLinkDownloadUrl($valueDownloadUrl); 
                            } 
                        }
                    }
                    elseif(in_array($key, ['ID', 'SECTION_ID', 'PRICE', 'CATALOG_ID'])) $arrResult['result'][$i][$key][] = intval($value);

                    elseif(!empty($value)) $arrResult['result'][$i][$key][] = str_replace(["\r\n", "\r", "\n", "\t"], '', strip_tags(strval($value)));

                    else $arrResult['result'][$i][$key][] = '';
                }
            }

            $arrResult['pagination'] = $this->paginations($getProducts['total'], 50, $start);
            
            return $arrResult;
            
        } catch (Exception $ex) {
            abort(500);
        }
    }

    private function paginations(int $total, int $count = 50, int $page): array
    {
        try {
            return [
                'currentPage' => ($page > 0) ? $page : 1,
                'pageCount' => (intval(ceil($total/$count)) > 0) ? intval(ceil($total/$count)) : 1,
                'maxCount' => $count, 
                'total' => $total
            ];
            
        } catch (Exception $e) {
            abort(500);
        }
    }

    public function getCrmProductFields(): array
    {   
        try {
            return $this->callMethod('crm.product.fields', []);
        } catch (Exception $e) {
            abort(500);
        }
    }
    
    private function refreshToken(): bool
    {
        try {

            $data = [
                'grant_type' => 'refresh_token',
                'client_id' => $this->getClientId(),
                'client_secret' => $this->getClientSecret(),
                'refresh_token' => $this->getRefreshToken()
            ];

            $result = $this->callCurl('https://oauth.bitrix.info/oauth/token/', http_build_query($data));

            if (isset($result['response']['access_token'], $result['response']['refresh_token'], $result['response']['member_id'])) {

                $this->accessToken = $result['response']['access_token'];
                
                $this->refreshToken = $result['response']['refresh_token'];

                UserActions::updateOrCreate(intval($this->callMethod('profile', [])['result'][ConstsRepository::getKeysBitrixProfile()['ID']]), $this->getMemberId(), $this->getAccessToken(), $this->getRefreshToken());

                return true;
            } 

            return false;
    
        }catch ( \Exception $ex) {
            abort(500);
        }
    }

    private function handlerResponce(array $result, string $method, array $params): array
    {
        try {
        
            $arrResult = [];

            $arrResult['http_code'] = $result['info']['http_code'];

            $success = true;

            if(isset($result['response']['error_description']) || isset($result['response']['error']) || $arrResult['http_code'] != 200) $success = false;

            $arrResult['status'] = $success;

            $arrResult = array_merge($result['response'], $arrResult);

            if(isset($arrResult["error_description"]) && (preg_match('/The access token provided has expired/', $arrResult["error_description"]) || preg_match('/expired_token/', $arrResult['error'])))
            {
                if($this->refreshToken() && $this->countRefresh === 0) 
                {
                    $this->countRefresh++;

                    return $this->callMethod($method, $params);
                }
            }

            if(!$success)
            {
                ErrorBitrixResourceCollection::setInfo(['Ошибка в битриксе', $result['response']['error_description'], $result['response']['error']]);
                throw new HttpResponseException(ErrorBitrixResourceCollection::responce());
            }
            
            self::logs($result);

            return $arrResult;

        } catch (Exception $e) {
            abort(500);
        }
    }


    /**
    *логи храняться 4 дня
    */
    private function logs(array $result): void
    {
        try {

            $files = Storage::directories('public/logs'); 

            $outdate = date('Y-m-d', strtotime('-4 day'));

            foreach($files as $key => $value){

                $dateDir = preg_replace('/public\/logs\//', '', $value);

                if (strtotime($dateDir) <= strtotime($outdate)) Storage::deleteDirectory($value);
            }

            if(!empty($result))
            {
                $path = 'logs/'.date('Y-m-d').'/'.$this->getMemberId().'/'.strtotime('now').'.json';

                if (Storage::disk('public')->missing($path)) Storage::disk('public')->put($path, json_encode($result));
            }
            
        } catch (Exception $e) {
            abort(500);
        }  
    }

    /**
    *метод возращает все данные не более 15 000
    */
    private function callMethodAll(string $method, array $params = []): array
    {
        try {

            $arrResult = [];

            $resultCurl[0] = static::callCurl("https://" . $this->getDomain() . "/rest/" . $method . "?access_token=" . $this->getAccessToken() . "&", http_build_query($params));

            $firstRequest = $this->handlerResponce($resultCurl[0], $method, $params);

            if(isset($firstRequest['total']) && ($firstRequest['total'] / 50) > 1)
            {   
                $iter = ceil($firstRequest['total'] / 50);

                for ($i = 1; $i < $iter; $i++)
                {   
                    if($iter <= 300)//не более 15 000 записей
                    {
                        $params['start'] = $i*50;

                        $resultCurl[$i] = static::callCurl("https://" . $this->getDomain() . "/rest/" . $method . "?access_token=" . $this->getAccessToken() . "&", http_build_query($params)); 
                    }
                }
            }

            if(!empty($resultCurl))
            {   
                $responceMethodName = $method;

                if($method == 'catalog.price.list') $responceMethodName = 'prices';//другое название не равное методу запроса

                for ($i = 0; $i < count($resultCurl); $i++)
                {
                    if(isset($resultCurl[$i]['response']['result'][$responceMethodName]))
                    {
                        foreach ($resultCurl[$i]['response']['result'][$responceMethodName] as $keyResult => $valueResult) {

                            $keyIter = $keyResult  + ( 50 * $i );

                            $arrResult['response']['result'][$responceMethodName][$keyIter] = $valueResult;
                        }
                    }
                }
            }

            return $arrResult; 
            
        } catch (Exception $e) {
            abort(500);
        }
    }

    /**
     * Метод выполняет одиночный запрос к REST API
     * Если обращение к списочным методам, вернет макс. 50 значений
    */
    private function callMethod(string $method, array $params = []): array
    {
        try {

            $result = static::callCurl("https://" . $this->getDomain() . "/rest/" . $method . "?access_token=" . $this->getAccessToken() . "&", http_build_query($params));

            return $this->handlerResponce($result, $method, $params);
            
        } catch (Exception $e) {
            abort(500);
        }
    }
}
