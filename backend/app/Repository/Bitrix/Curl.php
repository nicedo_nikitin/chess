<?php declare (strict_types = 1);

namespace App\Repository\Bitrix;

trait Curl
{
    public static function callCurl($url, $data)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        $info = curl_getinfo($ch);

        $result = [
            'response' => json_decode($response, true),
            'info' => $info,
        ];

        curl_close($ch);
        
        return $result;
    }
}