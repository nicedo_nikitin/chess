<?php
namespace App\Repository\Consts;

/**
*Класс хранения consts
*/
class ConstsRepository
{
	/**
    *Ключи $_REQEST при входе в приложение
    *@return array
    */
	public static function getKeysBitrixAuth(): array
	{
		return [
			'AUTH_ID' => 'AUTH_ID',
			'REFRESH_ID' => 'REFRESH_ID',
			'member_id' => 'member_id',
			'DOMAIN' => 'DOMAIN'
		];
	}

	/**
    *Поля по умолчанию в Битрикс24 по товарам
    *@return array
    */
	public static function getKeysBitrixProducts(): array
	{
		return [
			'ID' => 'ID',
			'SECTION_ID' => 'SECTION_ID',
			'CATALOG_ID' => 'CATALOG_ID',
			'NAME' => 'NAME',
			'CURRENCY_ID' => 'CURRENCY_ID', 
			'PRICE' => 'PRICE',
			'DESCRIPTION' => 'DESCRIPTION'
		];
	}

	/**
    *Поля по умолчанию в Битрикс24 по секциям
    *@return array
    */
	public static function getKeysBitrixSections(): array
	{
		return [
			'ID' => 'ID',
			'SECTION_ID' => 'SECTION_ID',
			'NAME' => 'NAME'
		];
	}

	/**
    *Поля по умолчанию в Битрикс24 по пользователю
    *@return array
    */
	public static function getKeysBitrixProfile(): array
	{
		return [
			'ID' => 'ID',
			'ADMIN' => 'ADMIN',
			'NAME' => 'NAME',
			'LAST_NAME' => 'LAST_NAME'
		];
	}
}