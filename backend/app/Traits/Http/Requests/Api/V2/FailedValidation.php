<?php
namespace App\Traits\Http\Requests\Api\V2;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;
use App\Http\Resources\Api\V2\Error\Validate\ErrorValidateCollection;

trait FailedValidation
{   
    protected function failedValidation(Validator $validator)
    {	
    	ErrorValidateCollection::setInfo($validator->errors()->toArray());
        throw new HttpResponseException(ErrorValidateCollection::responce());
    }
}
