<?php

namespace App\Traits\Http\Resources\Api\V2;

trait ErrorResponse
{   
    public static $setInfo = [];

    public static function setInfo(array $setInfo)
    {
        static::$setInfo = $setInfo;
    }

    public static function getStatus(): string
    {
        return 'internalServerError';
    }

    public static function getInfo(): array
    {
        return static::$setInfo;
    }

    public static function getStatusCode():int
    {
        return 500;
    }

    private static function data(): array
    {
        return ['data' => []];  
    } 

    public function toArray($request)
    {   
       return self::data();
    }

    private static function withResource()
    {
         return [
            'messages' => [
                'status' => self::getStatus(),
                'info' => self::getInfo() 
            ],
            'links' => [
                'self' => request()->url(),
            ],
        ];   
    }

    public function with($request)
    {
        return self::withResource();
    } 

    public function withResponse($request, $response)
    {   
        $response->setStatusCode(self::getStatusCode());
    }

    public static function responce()
    {
        return response()->json(
            array_merge(
                self::data(), 
                self::withResource()
            ), 
            self::getStatusCode()
        );
    }
}
