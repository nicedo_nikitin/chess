<?php

namespace App\Traits\Http\Resources\Api\V2;

trait SuccessResponse
{   

    public static function getStatus(): string
    {
        return 'ok';
    }

    public static function getInfo(): array
    {
        return [];
    }

    public static function getStatusCode():int
    {
        return 200;
    }

    public function toArray($request)
    {   
        return [
            'data' => isset($this->collection[0]) ? $this->collection[0] : [],
        ];
    }

    public function with($request)
    {       
        $arrResult = [];

        $arrResult = [
            'messages' => [
                'status' => self::getStatus(),
                'info' => self::getInfo() 
            ],
            'links' => [
                'self' => $request->url(),
            ]
        ];

        if(isset($this->collection[0]['pagination'])) $arrResult['pagination'] = $this->collection[0]['pagination'];

        return $arrResult;
    } 

    public static function responce()
    {
        return response()->json([
            $this->toArray(request()),
            self::with(request())    
        ], 
        self::getStatusCode()
        );
    }
}
