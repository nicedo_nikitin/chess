<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsFieldsPortalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_fields_portals', function (Blueprint $table) {
            $table->id();
            $table->foreignId('portal_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('field_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->string('bx_field_const');
        });

        Schema::table('settings_fields_portals', function (Blueprint $table) {
            $table->unique(['portal_id', 'field_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_fields_portals');
    }
}
