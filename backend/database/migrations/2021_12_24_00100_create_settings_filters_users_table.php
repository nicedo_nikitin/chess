<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsFiltersUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('settings_filters_users', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->index('user_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
            $table->foreignId('settings_fields_portal_id')->index('settings_fields_portal_id')->constrained()->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::table('settings_filters_users', function (Blueprint $table) {
            $table->unique(['user_id', 'settings_fields_portal_id'], 'user_setting_filter');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('settings_filters_users');
    }
}
