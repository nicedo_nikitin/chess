<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$Fields = [
    		[
    			'name' => 'цена',
    			'const' => 'price'
    		],
    		[
    			'name' => 'название',
    			'const' => 'name'
    		],
    		[
    			'name' => 'описание',
                'const' => 'description',
    		],
            [
                'name' => 'общая площадь',
                'const' => 'total_area',
            ],
            [
                'name' => 'этаж',
                'const' => 'floor',
            ],
            [
                'name' => 'этажи',
                'const' => 'floors',
            ],
    		[
    			'name' => 'изображения',
                'const' => 'images',
    		],
            [
                'name' => 'статус',
                'const' => 'status',
            ]
    	];

    	foreach($Fields as $Field)
    	{
    		\App\Models\Api\V2\Field::create($Field);
    	}
    }
}
