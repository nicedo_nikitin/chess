<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class ModuleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$Modules = [
            [
                'name' => 'недвижимость',
                'const' => 'realestate'
            ],
    	];

    	foreach($Modules as $Module)
    	{
    		\App\Models\Api\V2\Module::create($Module);
    	}
    }
}
