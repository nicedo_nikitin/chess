<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class PortalSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$portals = [
    		[	
    			'member_id' => '1e2292a6a5f9ab72bfe481e21456d2cf',
    			'domain' => 'eyJpdiI6ImpudGxmSlJLOUFLVkZVdmRNRklFSnc9PSIsInZhbHVlIjoiRlFCeVM0RTJqNVZUTjQzaFJzK3FnWVNWWmd4RWtnS3J4SHVjNEpJa0kyST0iLCJtYWMiOiJiYjQ1MzJlMGNiYTI0MzYxMTU0ZDk4NDBjY2FmMjFiZjI5NDZkYmFiYjA4M2M1NDBlZjBkNTM0ZTkwMmU5ZDg2IiwidGFnIjoiIn0='
		    ],
    	];

    	foreach($portals as $portal)
    	{
    		\App\Models\Api\V2\Portal::create($portal);
    	}
    }
}
