<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class TypeFieldSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$fields = \App\Models\Api\V2\Field::all();

        $types = \App\Models\Api\V2\Type::all();

        for($iField = 0; $iField < $fields->count(); $iField++)
        {   
            $TypeField = [];

            $fieldId = intval($fields[$iField]->id);

            if($fields[$iField]->const === 'price')
            {
                $TypeField[] = ['type_id' => intval($types->firstWhere('const', 'integer')->id), 'field_id' => $fieldId];
                $TypeField[] = ['type_id' => intval($types->firstWhere('const', 'double')->id), 'field_id' => $fieldId];
            }
            elseif(in_array($fields[$iField]->const,['name', 'description', 'status']))
            {
                $TypeField[] = ['type_id' => intval($types->firstWhere('const', 'string')->id), 'field_id' => $fieldId];
            }
            elseif($fields[$iField]->const === 'images')
            {
                $TypeField[] = ['type_id' => intval($types->firstWhere('const', 'product_file')->id), 'field_id' => $fieldId];
            }
            elseif(in_array($fields[$iField]->const, ['floor', 'floors']))
            {
                $TypeField[] = ['type_id' => intval($types->firstWhere('const', 'integer')->id), 'field_id' => $fieldId];
            }
            elseif(in_array($fields[$iField]->const, ['total_area']))
            {
                $TypeField[] = ['type_id' => intval($types->firstWhere('const', 'integer')->id), 'field_id' => $fieldId];
                $TypeField[] = ['type_id' => intval($types->firstWhere('const', 'double')->id), 'field_id' => $fieldId];
            }

            if(!empty($TypeField))
            {
                foreach ($TypeField as $valueTypeField) {
                   \App\Models\Api\V2\TypeField::create($valueTypeField);
                }
                
            }  
        }
    }
}
