<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$Types = [
            [
                'name' => 'целое число',
                'const' => 'integer'
            ],
    		[
    			'name' => 'дробное число',
    			'const' => 'double'
    		],
    		[
    			'name' => 'строка',
    			'const' => 'string'
    		],
    		[
    			'name' => 'фаил',
                'const' => 'product_file',
    		]
    	];

    	foreach($Types as $Type)
    	{
    		\App\Models\Api\V2\Type::create($Type);
    	}
    }
}
