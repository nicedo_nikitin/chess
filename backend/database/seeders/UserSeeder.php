<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	$users = [
    		[	
    			'portal_id' => 1,
		        'bx_id' => 1,
		        'refresh_token'  => 'eyJpdiI6ImxNZmVxcTMzR280KzR1SVE3NStnZWc9PSIsInZhbHVlIjoiclBTaG1nZVlKeWNoMmZJVlliYUdIbkxWa0pzc3N2bm5NalFJaWQwMi9XR1RmY3pzOXpBNkVJRVhPUG9hYkx4VzMrY2VkaXliUlpQMzhMOVdXQ3FWcnJhZFUrTDhxcjNyV1BrR1piSk9rdGs9IiwibWFjIjoiNzc3NDhlMTRhNmZmMjMxZWZmY2VhYTQzZGE1ZmEwMDc0ZjM3MmQyMWEyMTQxNmIyMTA2MDEzZjNkNjlhNWM5OSIsInRhZyI6IiJ9',
		        'access_token' => 'eyJpdiI6IjlYL210TEF2a3d0YUV4dWFyMEhiU3c9PSIsInZhbHVlIjoiMUhVMnlIUEZ1d3VpL1ovc1JybVg5MzdUVWx4TGVNYmh0eXdVbHltQXdQamVETmc4Y1Q1NkZER1NmelFjQXh1TFp6N3RjOHVDdW1VcVR3VEZIcjAySEs2K3YzVGJwbm96K3NzNlVGank0eDg9IiwibWFjIjoiYzYxNDQ4ODA1ZGNlYmRlNzk0MTY0YmZkMzlkNzJmNjkzMTNmN2FiNzA3Y2I3MTU2ODJmNDk0ZDZkZWNjYTAwYiIsInRhZyI6IiJ9',
                'module_id' => 1
		    ]
    	];

    	foreach($users as $user)
    	{
    		\App\Models\Api\V2\User::create($user);
    	}
    }
}
