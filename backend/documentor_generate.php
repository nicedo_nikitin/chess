<?php

require 'vendor/autoload.php';

class Documentor
{
    private $rootFolder = '';

    private $autoloadRoot = [];

    private $ignoreFolders = [];

    private $createFiles = [];

    private $groupsFiles = [];

    private $descProject = [];

    private $factory; 

    public function __construct(string $rootFolder, array $autoloadRoot, array $ignoreFolders, array $createFiles, array $descProject = [])
    {

        $this->rootFolder = $rootFolder;

        $this->autoloadRoot = $autoloadRoot;

        $this->descProject = $descProject;

        $this->ignoreFolders = $ignoreFolders;

        $this->createFiles = $createFiles;

        $this->factory  = \phpDocumentor\Reflection\DocBlockFactory::createInstance();
    }

    private function scanFolders(string $path,int $step = 1, string $group = '')
    {
        $listFolders = scandir($path);    
        
        foreach ($listFolders as $name) {

            if($name !== '.' && $name !== '..' )
            {
                $classPath = $this->getUseClass($path.'/'.$name);

                if($this->checkIgnoreFolder($classPath)) continue;

                if(preg_match("/.php/", $name))
                {
                    if(class_exists($classPath))
                    {
                        $reflection = new \ReflectionClass($classPath);   

                        $methods = $reflection->getMethods();

                        if($reflection->getDocComment() !== false) 
                        {
                            $this->groupsFilesInfo($group, 'mainComment', $classPath, $this->clearStringComment($reflection->getDocComment()));
                        }

                        if(is_array($methods) && !empty($methods))
                        {
                            for($i = 0; $i < count($methods); $i++)
                            {
                                if($reflection->getMethod($methods[$i]->name)->getDocComment() !== false && !$this->searchMethodsparent($reflection, $methods[$i]->name))
                                {
                                    $this->groupsFilesInfo($group, $methods[$i]->name, $classPath, $this->clearStringComment($reflection->getMethod($methods[$i]->name)->getDocComment()));
                                }
                            }
                        }
                    }  
                }
                elseif(file_exists($path.'/'.$name)) $this->scanFolders($path.'/'.$name, $step+1, ($step === 1) ? $name : $group);
            }
        }
    }

    private function checkIgnoreFolder(string $classPath): bool
    {
        foreach ($this->ignoreFolders as $folder) {

            if(preg_match("/".str_replace(["\\"], "\\\\", $folder)."/", $classPath)) return true;
        }

        return false;
    }

    private function searchMethodsparent(\ReflectionClass $reflection, string $method): bool
    {
        $getParentClass = $reflection->getParentClass();

        if(!empty($getParentClass))
        {
            if(!empty($getParentClass))
            {
                foreach ($getParentClass as $key => $nameClass) {

                    $reflectionParent = new \ReflectionClass($nameClass);

                    if(!empty($reflectionParent->getMethods()))
                    {   
                        $parentMethods = $reflectionParent->getMethods();

                        for($i = 0; $i < count($parentMethods); $i++)
                        {
                            if($parentMethods[$i]->name === $method && $parentMethods[$i]->name != '__construct') return true;
                        }
                    }
                }   
            } 
        }

        return false;
    }

    private function groupsFilesInfo(string $key, string $type, string $fileUse, string $info)
    {
        $this->groupsFiles[$key][$fileUse][$type][] = $info;
    }

    private function clearStringComment(string $comment): string
    {
        $comment  = str_replace(["/", "*", PHP_EOL, '/\s+/'], '', $comment); 

        return preg_replace('/\s+/', ' ', trim($comment));
    }

    private function getUseClass(string $pathClass): string
    {

        $pathClass = str_replace("/", '\\', $pathClass);

        $pathClass = str_replace("\\\\", '\\', $pathClass);

        $pathClass = str_replace([".php", "."], '', $pathClass);

        return str_replace("\\\\", '\\', array_key_first($this->autoloadRoot).explode($this->autoloadRoot[array_key_first($this->autoloadRoot)], $pathClass)[1]);   
    }

    private function generateFiles()
    {
        $createFiles = $this->createFiles;

        if(!empty($createFiles))
        {
            for($i = 0; $i < count($createFiles); $i++)
            {
                if($createFiles[$i]['type'] === 'md' OR $createFiles[$i]['type'] === 'html')
                {
                    file_put_contents($createFiles[$i]['path'].'/'.$createFiles[$i]['name'].'.'.$createFiles[$i]['type'], $this->getnerateStringMdFile());
                } 
            }
        }
    }

    private function getnerateStringMdFile(): string
    {
        $strResult = '';

        $groupsFiles = $this->groupsFiles;

         $strResult .= '<h2>Дата создания описания: '.date('d.m.Y H:i:s').'</h2>';

        $strResult .= "<h2>Описание проекта:</h2>".implode("<br>",$this->descProject);

        foreach ($groupsFiles as $type => $files) {
            
            $strResult .= '<h2>Тип файла '.$type.'</h2>';

            if(is_array($files))
            {
                foreach ($files as $use => $methods) {
                    
                    $strResult .= "<h3>use ".$use."</h3>"; 
                    
                    if(is_array($methods))
                    {
                        foreach ($methods as $method => $descriptions) {
                            
                            $nameMethod = '<span>Метод: '.$method.'</span>';

                            if($method == 'mainComment') $nameMethod = 'Описание класса:';

                            $strResult .= '<b><span>'.$nameMethod.'</span></b>'; 

                            $strResult .= '<p>'.implode('<br>', $descriptions).'</p>';
                        }
                    }
                }
            } 
        }

        return $strResult;
    }


    public function create()
    {
        $this->scanFolders($this->rootFolder);

        $this->generateFiles();
    }
}


function dd($arr)
{
    echo '<pre>';
        var_dump($arr);
    echo '</pre>';
    die();
}

function createDocumentation()
{
    $ignoreFolders = [
        'App\Http\Middleware',
        'App\Http\Kernel',
        'App\Http\Requests',
        'App\Http\Resource',
        'App\Console',
        'App\Exceptions',
        'App\Models',
        'App\Tools',
        'App\Providers',
        'App\Traits',
    ];

    $createFiles = [['name' => 'README', 'type'=> 'md', 'path' => './']];

    $desc = [];

    $desc[] = 'Приложение "Шахматка, модуль Недвижимость" - главный инструмент работы менеджера по продажам строительной организации (застройщика).';
    $desc[] = 'Предназначен для автоматизиции процесса продаж и создания единого пространства, где наглядно отображается актуальная информация по объектам недвижимости.';

    $desc[] = 'Приложение "Шахматка, модуль Недвижимость" выполняет визуализацию товарного каталога CRM Битрикс24.';

    $desc[] = 'У приложения существует настройка по умолчанию, которая позволяет запускать приложение сразу после установки.';

    $desc[] = 'Основной функционал приложения:';
    $desc[] = 'Визуализация товаров:';
    $desc[] = '- показ товаров по фильтру, позволит легко отобразить только интересующие товары';
    $desc[] = '- показ товаров из любой папки в товарном каталоге (если они существуют), без привязки к структуре и вложенности';
    $desc[] = '- показ детальной информации по товарам, дополнительно включает в себя слайдер и галерею для изображений ';
    $desc[] = '- модуль печати, позволяет сохранить .pdf файл или распечатать документ с мини-карточками товаров или с детальной информацией о товаре';
    $desc[] = 'Настройка:';
    $desc[] = 'Глобальная настройка портала:';
    $desc[] = '- связывание полей товаров в приложении с полями из CRM Битрикс24';
    $desc[] = 'Пользовательские настройки (настраиваются индивидуально каждым пользователем Шахматки):';
    $desc[] = '- настройка отображения полей фильтра';
    $desc[] = '- настройка отображения/сортировки полей мини-карточки товара';
    $desc[] = '- настройка отображения/сортировки полей детальной карточки товара';

    $desc[] = 'Наглядность и оперативность, удобство и экономия времени. В момент обращения клиента менеджер может сразу отфильтровать увидеть и показать необходимые клиенту товары, применить опции и ';'предоставить всю необходимую информацию. Все, что нужно знать о товарах у Вас перед глазами - в одном окне. Встроена возможность вывести на печать документ, в котором будет отражена ';'вся актуальная информация об объекте, фактически формируя коммерческое предложение.';

    $documentor = new Documentor(__DIR__.'/app', ['App' => 'app'], $ignoreFolders, $createFiles, $desc);

    $documentor->create();
}

createDocumentation();

