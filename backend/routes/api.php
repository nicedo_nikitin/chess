<?php
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V2\PortalController;
use App\Http\Controllers\Api\V2\UserController;
use App\Http\Controllers\Api\V2\SectionController;
use App\Http\Controllers\Api\V2\FilterController;
use App\Http\Controllers\Api\V2\ProductController;
use App\Http\Controllers\Api\V2\SettingsFieldsPortalController;
use App\Http\Controllers\Api\V2\SettingMinicardUserController;
use App\Http\Controllers\Api\V2\SettingDetailcardUserController;
use App\Http\Controllers\Api\V2\SettingFilterUserController;

Route::group([
    'prefix' => 'v3/bitrix/realestate'
], function ($router) {
		
	Route::get('auth',  function () {
    	return 'Hello bitrix';
	});

    Route::post('auth', [UserController::class,'auth']);
    Route::get('refresh', [UserController::class,'refresh']);

    Route::group(['middleware' => ['auth:api', 'portalSettings', 'userSettings']], function(){

    	Route::group(['prefix' => 'visuals'], function ($router) {
    		
    		Route::get('user', [UserController::class,'show']);
    		
    		Route::get('sections/{sectionId}', [SectionController::class,'show'])->where('sectionId', '[0-9]+');
	   		
	   		Route::post('products/{page}', [ProductController::class,'index'])->where('page', '[0-9]+');
	   		
	   		Route::get('products/{productId}', [ProductController::class,'show'])->where('productId', '[0-9]+');

	   		Route::get('filter', [FilterController::class,'index']);
    	});
	   	
	   	Route::group(['prefix' => 'settings'], function ($router) {
	   		
	   		Route::group(['middleware' => 'bitrixAdmin'], function ($router) {
		   		Route::get('fields', [SettingsFieldsPortalController::class,'index']);
		   		Route::put('fields/{field}', [SettingsFieldsPortalController::class,'updateOrCreate'])->where(['field' => '[0-9]+']);
		   		Route::delete('fields/{field}', [SettingsFieldsPortalController::class, 'destroy'])->where(['field' => '[0-9]+']);
	   		});

	   		Route::get('minicard', [SettingMinicardUserController::class,'index']);
		   	Route::put('minicard/{field}', [SettingMinicardUserController::class,'deleteOrCreate'])->where(['field' => '[0-9]+']);
		   	Route::put('minicard/{field}/order/{order}', [SettingMinicardUserController::class, 'order'])->where(['field' => '[0-9]+', 'order' => '[0-9]+']);

		   	Route::get('detailcard', [SettingDetailcardUserController::class,'index']);
		   	Route::put('detailcard/{field}', [SettingDetailcardUserController::class,'deleteOrCreate'])->where(['field' => '[0-9]+']);
		   	Route::put('detailcard/{field}/order/{order}', [SettingDetailcardUserController::class, 'order'])->where(['field' => '[0-9]+', 'order' => '[0-9]+']);

		   	Route::get('filter', [SettingFilterUserController::class,'index']);
		   	Route::put('filter/{field}', [SettingFilterUserController::class,'deleteOrCreate'])->where(['field' => '[0-9]+']);
	   	});
	});
});
