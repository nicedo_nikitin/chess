<?php
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Artisan;
use App\Http\Controllers\Web\V2\BitrixController;
use Illuminate\Http\Request;
/**
*init bitrix app
*/
Route::any('bitrix/install', [BitrixController::class, 'install']);

Route::any('bitrix/view', function (Request $request) {
    return view('bitrix');
});