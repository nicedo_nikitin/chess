export default class Printer {
  static execute(sectionName) {
    if (process.browser) {
      const detailFilterPrint = document.querySelector(`.${sectionName}__filter-print`)
      const detailSectionsPrint = document.querySelector(`.${sectionName}__sections-print`)
      const filters = document.querySelectorAll('.filter__input')
      const sectionBtns = document.querySelectorAll('.sections__btn--active')

      if (detailFilterPrint) {
        detailFilterPrint.innerHTML = null

        for (let i = 0; i < filters.length; i++) {
          if (filters[i].value) {
            let filterDiv = document.createElement('div')
            filterDiv.textContent = filters[i].getAttribute('name') + ' - ' + filters[i].value
            detailFilterPrint.appendChild(filterDiv)
          }
        }
      }

      if (detailSectionsPrint) {
        detailSectionsPrint.innerHTML = null

        for (let i = 0; i < sectionBtns.length; i++) {
          let sectionDiv = document.createElement('div')
          sectionDiv.textContent = sectionBtns[i].textContent + ' ->'
          detailSectionsPrint.appendChild(sectionDiv)
        }
      }

      if (detailSectionsPrint.lastChild) {
        detailSectionsPrint.lastChild.textContent = detailSectionsPrint.lastChild.textContent.slice(
          0,
          -3
        )
      }

      window.print()
    }
  }
}
