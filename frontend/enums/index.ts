export enum EProgramPlatform {
  bitrix = 'bitrix',
  web = 'web'
}

export enum EProgramModule {
  realestate = 'realestate'
}

export enum EProgramContext {
  visuals = 'visuals',
  settings = 'settings',
  defaults = 'defaults',
  refresh = 'refresh'
}

export enum EProgramEssence {
  products = 'products',
  sections = 'sections',
  fields = 'fields',
  minicard = 'minicard',
  detailcard = 'detailcard',
  filter = 'filter',
  user = 'user'
}

export enum EMethod {
  GET = 'GET',
  POST = 'POST',
  PUT = 'PUT',
  PATCH = 'PATCH',
  DELETE = 'DELETE'
}

export enum EResponseStatus {
  ok = 200,
  unauthorized = 401,
  badRequest = 400,
  forbidden = 403,
  tooManyRequests = 429
}

export enum EDevice {
  desktop = 'desktop',
  mobile = 'mobile'
}
