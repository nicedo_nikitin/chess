export default interface IFilter {
  [key: string]: string | number
}
