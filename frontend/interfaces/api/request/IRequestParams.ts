import { EMethod } from '@/enums'

export default interface IRequestParams {
  method: EMethod
  url: string
  data?: any
  successCallback: any
  errorCallback: any
}
