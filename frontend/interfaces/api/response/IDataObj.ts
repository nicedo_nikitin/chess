import { EProgramEssence } from '@/enums'

export default interface IDataObj {
  type: EProgramEssence
  id?: number
  attributes: {
    [key: string]: any // данные объекта
  }
  relationships?: {
    // связанные сущности
    [key: string]: {
      links: {
        self: string
        related: string
      }
      data: {
        type: EProgramEssence
        id: number
      }
    }
  }
}
