import { EResponseStatus } from '@/enums'
import IDataObj from '@/interfaces/api/response/IDataObj'

export default interface IResponse {
  data: {
    messages: {
      status: EResponseStatus
      info: Array<string>
    }
    data: Array<IDataObj>
    links: {
      self: string
    }
    pagination?: {
      maxCount: number
      currentPage: number
      pageCount: number
      total: number
    }
  }
  status: number
}
