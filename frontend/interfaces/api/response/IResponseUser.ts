import { EResponseStatus, EProgramEssence } from '@/enums'

export default interface IResponse {
  data: {
    messages: {
      status: EResponseStatus
      info: Array<string>
    }
    data: {
      attributes: {
        admin: boolean
        bxId: number
        firstName: string
        lastName: string
      }
      id: number
      type: EProgramEssence
    }
    links: {
      self: string
    }
    pagination?: {
      maxCount: number
      currentPage: number
      pageCount: number
      total: number
    }
  }
  status: number
}
