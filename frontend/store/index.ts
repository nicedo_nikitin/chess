import { Commit, Dispatch } from 'vuex'
import axios from 'axios'
import { EProgramPlatform, EProgramModule, EProgramContext, EResponseStatus } from '@/enums'
import IFilter from '@/interfaces/api/request/IFilter'
import IRequestParams from '@/interfaces/api/request/IRequestParams'

export const state = () => ({
  clientVersion: '2.0.0' as string,
  platform: '' as EProgramPlatform,
  module: '' as EProgramModule,
  apiUrl: 'https://broking.su/apps/chessv3/backend/public/api/v3' as string,
  isAdmin: false as boolean,
  tokenRefreshLimit: 0 as number,
  programCrashed: false as boolean,
  currentSection: 0 as number,
  sectionRefresh: false as boolean,
  boundFieldChanged: false as boolean,
  detailProductState: false as boolean,
  detailProductId: 0 as number,
  filterSubmitted: false as boolean,
  filterReset: false as boolean,
  filterRequest: {} as IFilter
})

export const getters = {
  getClientVersion(state: any): string {
    return state.clientVersion
  },
  getPlatform(state: any): EProgramPlatform {
    return state.platform
  },
  getModule(state: any): EProgramModule {
    return state.module
  },
  getApiUrl(state: any): string {
    return state.apiUrl
  },
  getIsAdmin(state: any): boolean {
    return state.isAdmin
  },
  getTokenRefreshLimit(state: any): number {
    return state.tokenRefreshLimit
  },
  getProgramCrashed(state: any): boolean {
    return state.programCrashed
  },
  getCurrentSection(state: any): number {
    return state.currentSection
  },
  getSectionRefresh(state: any): boolean {
    return state.sectionRefresh
  },
  getBoundFieldChanged(state: any): boolean {
    return state.boundFieldChanged
  },
  getDetailProductState(state: any): boolean {
    return state.detailProductState
  },
  getDetailProductId(state: any): number {
    return state.detailProductId
  },
  getFilterSubmitted(state: any): boolean {
    return state.filterSubmitted
  },
  getFilterReset(state: any): boolean {
    return state.filterReset
  },
  getFilterRequest(state: any): IFilter {
    return state.filterRequest
  }
}

export const mutations = {
  setPlatform(state: any, platform: EProgramPlatform): void {
    state.platform = platform
  },
  setModule(state: any, module: EProgramModule): void {
    state.module = module
  },
  setApiUrl(state: any, url: string): void {
    state.apiUrl = url
  },
  setIsAdmin(state: any, flag: boolean): void {
    state.isAdmin = flag
  },
  incrementTokenRefreshLimit(state: any): void {
    state.tokenRefreshLimit = state.tokenRefreshLimit + 1
  },
  setProgramCrashed(state: any, flag: boolean): void {
    state.programCrashed = flag
  },
  setCurrentSection(state: any, order: number): void {
    state.currentSection = order
  },
  setSectionRefresh(state: any, flag: boolean): void {
    state.sectionRefresh = flag
  },
  setBoundFieldChanged(state: any, flag: boolean): void {
    state.boundFieldChanged = flag
  },
  setDetailProductState(state: any, flag: boolean): void {
    state.detailProductState = flag
  },
  setDetailProductId(state: any, productId: number): void {
    state.detailProductId = productId
  },
  setFilterSubmitted(state: any, flag: boolean): void {
    state.filterSubmitted = flag
  },
  setFilterReset(state: any, flag: boolean): void {
    state.filterReset = flag
  },
  setFilterRequest(state: any, request: IFilter): void {
    state.filterRequest = request
  }
}

export const actions = {
  async sendRequest(
    { state, commit, dispatch }: { state: any; commit: Commit; dispatch: Dispatch },
    requestAction: IRequestParams
  ): Promise<void> {
    const catchAuthExpired = async (error: any): Promise<void> => {
      if (error.response.status === EResponseStatus.unauthorized) {
        const refresh = await dispatch('refreshAuth')

        if (refresh) {
          await dispatch('sendRequest', requestAction)
        } else {
          commit('setProgramCrashed', true)
          return
        }

        if (state.tokenRefreshLimit > 50) {
          commit('setProgramCrashed', true)
          return
        }

        return
      }

      throw error
    }

    switch (requestAction.method) {
      case 'GET':
        await axios
          .get(requestAction.url)
          .then(requestAction.successCallback)
          .catch(catchAuthExpired)
          .catch(requestAction.errorCallback)
        break
      case 'POST':
        await axios
          .post(requestAction.url, requestAction.data)
          .then(requestAction.successCallback)
          .catch(catchAuthExpired)
          .catch(requestAction.errorCallback)
        break
      case 'PUT':
        await axios
          .put(requestAction.url, requestAction.data)
          .then(requestAction.successCallback)
          .catch(catchAuthExpired)
          .catch(requestAction.errorCallback)
        break
      case 'PATCH':
        await axios
          .patch(requestAction.url, requestAction.data)
          .then(requestAction.successCallback)
          .catch(catchAuthExpired)
          .catch(requestAction.errorCallback)
        break
      case 'DELETE':
        await axios
          .delete(requestAction.url, requestAction.data)
          .then(requestAction.successCallback)
          .catch(catchAuthExpired)
          .catch(requestAction.errorCallback)
        break
    }
  },
  async refreshAuth({ state, commit }: { state: any; commit: Commit }): Promise<boolean> {
    let refresh: boolean = false

    await axios
      .get(`${state.apiUrl}/${EProgramContext.refresh}`)
      .then(() => {
        refresh = true
      })
      .catch(() => {
        refresh = false
      })

    commit('incrementTokenRefreshLimit')

    return refresh
  }
}
